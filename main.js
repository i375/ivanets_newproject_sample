/// <reference path="../IvaneMain.ts" />
/*
 * Author Ivane Gegia http://ivane.info
 */
var Ivane;
(function (Ivane) {
    var Network;
    (function (Network) {
        var Ajax;
        (function (Ajax) {
            (function (REQUEST_TYPES) {
                REQUEST_TYPES[REQUEST_TYPES["GET"] = 0] = "GET";
                REQUEST_TYPES[REQUEST_TYPES["POST"] = 1] = "POST";
            })(Ajax.REQUEST_TYPES || (Ajax.REQUEST_TYPES = {}));
            var REQUEST_TYPES = Ajax.REQUEST_TYPES;
            (function (DATA_TYPES) {
                DATA_TYPES[DATA_TYPES["SCRIPT"] = 0] = "SCRIPT";
                DATA_TYPES[DATA_TYPES["JSON"] = 1] = "JSON";
                DATA_TYPES[DATA_TYPES["XML"] = 2] = "XML";
                DATA_TYPES[DATA_TYPES["HTML"] = 3] = "HTML";
                DATA_TYPES[DATA_TYPES["TEXT"] = 4] = "TEXT";
            })(Ajax.DATA_TYPES || (Ajax.DATA_TYPES = {}));
            var DATA_TYPES = Ajax.DATA_TYPES;
            function getStringForREQUEST_TYPES(requestType) {
                switch (requestType) {
                    case REQUEST_TYPES.GET:
                        return "GET";
                        break;
                    case REQUEST_TYPES.POST:
                        return "POST";
                        break;
                }
            }
            var AJAX_READY_STATES;
            (function (AJAX_READY_STATES) {
                AJAX_READY_STATES[AJAX_READY_STATES["REQUEST_NOT_INITIALIZED"] = 0] = "REQUEST_NOT_INITIALIZED";
                AJAX_READY_STATES[AJAX_READY_STATES["SERVER_CONNECTION_ESTABLISHED"] = 1] = "SERVER_CONNECTION_ESTABLISHED";
                AJAX_READY_STATES[AJAX_READY_STATES["REQUEST_RECEIVED"] = 2] = "REQUEST_RECEIVED";
                AJAX_READY_STATES[AJAX_READY_STATES["PROCESSING_REQUEST"] = 3] = "PROCESSING_REQUEST";
                AJAX_READY_STATES[AJAX_READY_STATES["REQUEST_FINISHED_AND_RESPOSNE_IS_READY"] = 4] = "REQUEST_FINISHED_AND_RESPOSNE_IS_READY";
            })(AJAX_READY_STATES || (AJAX_READY_STATES = {}));
            var AJAX_REQUEST_STATUS_CODES;
            (function (AJAX_REQUEST_STATUS_CODES) {
                AJAX_REQUEST_STATUS_CODES[AJAX_REQUEST_STATUS_CODES["OK"] = 200] = "OK";
                AJAX_REQUEST_STATUS_CODES[AJAX_REQUEST_STATUS_CODES["PAGE_NOT_FOUND"] = 404] = "PAGE_NOT_FOUND";
            })(AJAX_REQUEST_STATUS_CODES || (AJAX_REQUEST_STATUS_CODES = {}));
            function createAJAXRequest(url, requestType, data_nullable, onSuccess, onFail) {
                var ajaxRequest = new XMLHttpRequest();
                var requestTypeString = getStringForREQUEST_TYPES(requestType);
                var queryString = url;
                var urlEncodedParameters = "";
                if (data_nullable != null) {
                    if (data_nullable instanceof Object) {
                        var data_keys = Object.keys(data_nullable);
                        for (var data_key_index = 0; data_key_index < data_keys.length; data_key_index++) {
                            var data_key = data_keys[data_key_index];
                            urlEncodedParameters += data_key
                                + "="
                                + data_nullable[data_key]
                                + "&";
                        }
                    }
                    else if (data_nullable instanceof String) {
                        urlEncodedParameters = data_nullable;
                    }
                    urlEncodedParameters = urlEncodedParameters.substr(0, urlEncodedParameters.length - 2);
                    if (urlEncodedParameters.charAt(0) != "?"
                        && url.charAt(-1) != "?") {
                        urlEncodedParameters = "?" + urlEncodedParameters;
                    }
                }
                if (requestType == REQUEST_TYPES.GET) {
                    ajaxRequest.open(getStringForREQUEST_TYPES(requestType), url + urlEncodedParameters, true);
                    ajaxRequest.send();
                }
                else if (requestType == REQUEST_TYPES.POST) {
                    ajaxRequest.open(getStringForREQUEST_TYPES(requestType), url, true);
                    ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                    ajaxRequest.send(urlEncodedParameters.replace("?", ""));
                }
                ajaxRequest.onreadystatechange = function (ev) {
                    if (ajaxRequest.readyState == AJAX_READY_STATES.REQUEST_FINISHED_AND_RESPOSNE_IS_READY
                        && ajaxRequest.status == 200) {
                        var ajaxReponse = {
                            responseBody: ajaxRequest.responseBody,
                            responseText: ajaxRequest.responseText
                        };
                        onSuccess(ajaxReponse);
                    }
                    else if (ajaxRequest.readyState == AJAX_READY_STATES.REQUEST_FINISHED_AND_RESPOSNE_IS_READY
                        && ajaxRequest.status != 200) {
                        onFail(ev);
                    }
                };
                ajaxRequest.onerror = function (ev) {
                    onFail(ev);
                };
                return ajaxRequest;
            }
            Ajax.createAJAXRequest = createAJAXRequest;
        })(Ajax = Network.Ajax || (Network.Ajax = {}));
    })(Network = Ivane.Network || (Ivane.Network = {}));
})(Ivane || (Ivane = {}));
/*
 * Author Ivane Gegia http://ivane.info
 */
var Ivane;
(function (Ivane) {
    var Animation;
    (function (Animation_1) {
        (function (EASING_TYPES) {
            EASING_TYPES[EASING_TYPES["LINEAR"] = 0] = "LINEAR";
            EASING_TYPES[EASING_TYPES["EASE_IN_EASE_OUT"] = 1] = "EASE_IN_EASE_OUT";
        })(Animation_1.EASING_TYPES || (Animation_1.EASING_TYPES = {}));
        var EASING_TYPES = Animation_1.EASING_TYPES;
        /**
         * ანიმაციის აღმწერი კლასი.
         * ამ კლასში ხდება ანიმაციის კოეფიციენტების გადათვლა, შესაბამისი easing-ის ფუქნციების გამოყენებით.
         */
        var Animation = (function () {
            function Animation() {
                this.from = 0;
                this.to = 0;
                this.active = false;
                this.normalizedProgress = 0;
            }
            Animation.prototype.getNormalizedProgressEasingApplied = function () {
                switch (this.easingType) {
                    case EASING_TYPES.LINEAR:
                        return this.normalizedProgress;
                    case EASING_TYPES.EASE_IN_EASE_OUT:
                        var easingApplied = (Math.sin(this.normalizedProgress * Math.PI - Math.PI / 2) + 1) / 2;
                        return easingApplied;
                    default:
                        return 0.0;
                }
            };
            /**
             * აბრუნებს მიმდინარე გპროგრესს, მაგალითად თუ ანიმაცია იყო 0-დან 10-მდე, დააბრუნებს 0-დან 10-მდე,
             * მიმდინარე კადრისათვის შესაბამის მნიშვნელობას.
             */
            Animation.prototype.getProgress = function () {
                return this.from + (this.to - this.from) * this.getNormalizedProgressEasingApplied();
            };
            /**
             * გამოიყენება {AnimationsManager}-ის მიერ ანიმაციის სამართავად.
             */
            Animation.prototype.stepAnimation = function (deltaTime) {
                this.normalizedProgress += deltaTime / this.durationInSeconds;
                if (this.normalizedProgress > 1.0) {
                    this.normalizedProgress = 1.0;
                    this.animationStepDelegate(this);
                    this.animationFinishedDelegate(this);
                    this.active = false;
                }
                else {
                    this.animationStepDelegate(this);
                }
            };
            /**
             * ანიმაციის განულება, გამოიყენება {AnimationsManager}-ის მიერ.
             * მეთოდის არსი მდგომარეობს იმაში რომ {AnimationsManager}-ი ინახავს ანიმაციების რიგს(pool),
             * რომლიდანაც ხელახლა გამოყენების პრინციპით იღებს ანიმაციების ობიექტებს ანულებს მათ
             * და ახლად დამატებული მნიშვნელობების ითვლის მისით.
             */
            Animation.prototype.reset = function (from, to, durationInSeconds, easingType, animationStepDelegate, animationFinishedDelegate) {
                this.from = from;
                this.to = to;
                this.durationInSeconds = durationInSeconds;
                this.active = true;
                this.animationStepDelegate = animationStepDelegate;
                this.animationFinishedDelegate = animationFinishedDelegate;
                this.normalizedProgress = 0;
                this.easingType = easingType;
            };
            return Animation;
        })();
        Animation_1.Animation = Animation;
        /**
         * ანიმაციების მენეჯერი.
         * გამოიყენება ანიმაციების რიგის შესაქმნელად და მასში ახალი ანიმაციის ჩასამატებლად.
         * ასევე უზრუნველყოფს ანიამციების განახლება/გადათვლას.
         */
        var AnimationsManager = (function () {
            function AnimationsManager(animationsPoolSize) {
                this.createAnimationsPool(animationsPoolSize);
            }
            /**
             * @param animationsPoolSize ანიმაციების რიგის ზომა, ანუ რამდენი ანიმაცია შეიძლება მაქსიმუმ ერთდოულად მართოს {AnimationsManager}-მა
             */
            AnimationsManager.prototype.createAnimationsPool = function (animationsPoolSize) {
                this.animationsPool = new Array(animationsPoolSize);
                for (var animationIndex = 0; animationIndex < animationsPoolSize; animationIndex++) {
                    this.animationsPool[animationIndex] = new Animation();
                }
            };
            /**
             * ანიმაციას აყენებს შესასრულებლების რიგში.
             *
             * @param from ანიმაციის საწყისი მნიშვნელობა
             * @param to ანიმაციის საბოლოო მნიშვნელობა
             * @param durationInSeconds ანიმაციის ხანგრძლივობა წამებში
             * @param easingType იზინგის ტიპი
             * @param animationStepHandler ანიმაციის ბიჯის დამმუშავებელი ფუნქცია, ამ ფუნქციას გადმოიცემა მიმდინარე ანიმაცია, მისი მნიშნველობების გამოსაყენებლად
             * @param animationFinishedHandler ანიმაციის დასასრულის დამმუშავებელი ფუნქცია, ამ ფუნქციას გადმოიცემა მიმდინარე ანიმაცია, მისი მნიშნველობების გამოსაყენებლად
             */
            AnimationsManager.prototype.queueAnimation = function (from, to, durationInSeconds, easingType, animationStepHandler, animationFinishedHandler) {
                var animation = this.tryToGetInactiveAnimation();
                animation.reset(from, to, durationInSeconds, easingType, animationStepHandler, animationFinishedHandler);
            };
            AnimationsManager.prototype.queueFunctionCall = function (inSeconds, func) {
                this.queueAnimation(0.0, 1.0, inSeconds, Ivane.Animation.EASING_TYPES.LINEAR, function (anim) { }, function (anim) {
                    func();
                });
            };
            /**
             * ცდის მოიპოვოს თავისუფლაი ანიმაციის ობიექტი, ანიმაციების რიგიდან.
             */
            AnimationsManager.prototype.tryToGetInactiveAnimation = function () {
                var animation = null;
                for (var animationIndex = 0; animationIndex < this.animationsPool.length; animationIndex++) {
                    animation = this.animationsPool[animationIndex];
                    if (animation.active == false) {
                        break;
                    }
                }
                if (animation.active == true) {
                    animation = null;
                }
                return animation;
            };
            /**
             * აახლებს ანიმაციებს.
             */
            AnimationsManager.prototype.stepAnimations = function (deltaTime) {
                for (var animationIndex = 0; animationIndex < this.animationsPool.length; animationIndex++) {
                    var animation = this.animationsPool[animationIndex];
                    if (animation.active) {
                        animation.stepAnimation(deltaTime);
                    }
                }
            };
            /**
             * ფუნქცია უნდა გმოიძახო gameStep მეთოდში, როცა სამყაროს გადათვლას აკეთებ.
             * მაგალითად: this.animationsManager1.update(this.deltaTime)
             *
             * @param deltaTime დრო კადრსა და კადრს შორის
             */
            AnimationsManager.prototype.update = function (deltaTime) {
                this.stepAnimations(deltaTime);
            };
            return AnimationsManager;
        })();
        Animation_1.AnimationsManager = AnimationsManager;
    })(Animation = Ivane.Animation || (Ivane.Animation = {}));
})(Ivane || (Ivane = {}));
/*
 * Author Ivane Gegia http://ivane.info
 */
var Ivane;
(function (Ivane) {
    var Exceptions;
    (function (Exceptions) {
        function MethodNotOverriden() { }
        Exceptions.MethodNotOverriden = MethodNotOverriden;
        function NotImplemetedException() { }
        Exceptions.NotImplemetedException = NotImplemetedException;
        function DynamicAssertionError() { }
        Exceptions.DynamicAssertionError = DynamicAssertionError;
        function VertexNumberOutOfVerexBuffer() { }
        Exceptions.VertexNumberOutOfVerexBuffer = VertexNumberOutOfVerexBuffer;
    })(Exceptions = Ivane.Exceptions || (Ivane.Exceptions = {}));
})(Ivane || (Ivane = {}));
/*
 * Author Ivane Gegia http://ivane.info
 */
/// <reference path="Exceptions.ts" />
var Ivane;
(function (Ivane) {
    var Assertion;
    (function (Assertion) {
        function DynamicAssert(condition, errorMessage) {
            if (!condition) {
                console.log(errorMessage);
                throw new Ivane.Exceptions.DynamicAssertionError();
            }
        }
        Assertion.DynamicAssert = DynamicAssert;
    })(Assertion = Ivane.Assertion || (Ivane.Assertion = {}));
})(Ivane || (Ivane = {}));
/*
 * Author Ivane Gegia http://ivane.info
 */
///<reference path="Exceptions.ts"/>
var Ivane;
(function (Ivane) {
    var Inputs;
    (function (Inputs) {
        var MouseXY = (function () {
            function MouseXY() {
                this.x = 0;
                this.y = 0;
            }
            return MouseXY;
        })();
        Inputs.MouseXY = MouseXY;
        (function (MOUSE_BUTTONS) {
            MOUSE_BUTTONS[MOUSE_BUTTONS["LEFT"] = 1] = "LEFT";
            MOUSE_BUTTONS[MOUSE_BUTTONS["RIGHT"] = 2] = "RIGHT";
            MOUSE_BUTTONS[MOUSE_BUTTONS["MIDDLE"] = 4] = "MIDDLE";
        })(Inputs.MOUSE_BUTTONS || (Inputs.MOUSE_BUTTONS = {}));
        var MOUSE_BUTTONS = Inputs.MOUSE_BUTTONS;
        var CanvasInputsManager = (function () {
            function CanvasInputsManager() {
                /**
                 * მაუსს ხელი აეშვა ამ კადრში
                 */
                this.mouseUp = false;
                /**
                 * მაუსი ახლა დაჭერილია თუ არა
                 */
                this.mouseIsDown = false;
                /**
                 * მაუსს დაეჭირა ამ კადრში
                 */
                this.mouseDown = false;
                /**
                 * მაუსი გამოძრავდა თუ არა, წინა კადრის პოზიციასთან შედარებით
                 */
                this.mouseMoved = false;
                this.mouseButonsBitMap = 0;
                this.realTimeMouseIsDown = false;
                this.realTimeMouseClicked = false;
                this.realTimeMouseIsUp = true;
                this.mouseDownRegisteredOnce = false;
                this.mousePreviousScreenXY = new MouseXY();
                this.mouseCurrentScreenXY = new MouseXY();
                this.realTimeMouseXY = new MouseXY();
                this.mouseXY = new MouseXY();
                this.mouseDeltaXY = new MouseXY();
                this.mouseDeltaXYAccumulator = new MouseXY();
                this.initKeyProcessingBuffers();
            }
            CanvasInputsManager.prototype.initKeyProcessingBuffers = function () {
                this.keysWhichAreDown = new Array(KeyCodes.single_quote);
                this.keysPreviousFrameState = new Array(KeyCodes.single_quote);
                for (var keyIndex = 0; keyIndex < this.keysWhichAreDown.length; keyIndex++) {
                    this.keysPreviousFrameState[keyIndex] = false;
                    this.keysWhichAreDown[keyIndex] = false;
                }
            };
            /**
             *
             * კლავიშებისთვის, მიმდინარე კადრის მდგომარეობები გადააქვს წინა კადრის შემნახველ ბუფერში.
             * მაგალითად: წინა კადრში არ იყო დაჭერილი, ახლა დაჭერილია, შემდეგ კადრში წინა კადრი იქნება დაჭერილი და მიმდინარე ის რაც იქნება.
             *
             */
            CanvasInputsManager.prototype.copyCurrentKeyStatesToPreviousFrameStates = function () {
                for (var keyIndex = 0; keyIndex < this.keysWhichAreDown.length; keyIndex++) {
                    this.keysPreviousFrameState[keyIndex] = this.keysWhichAreDown[keyIndex];
                }
            };
            /**
             *
             * აბრუნებს კლავიშის მდგომარეობას წინა კადრში.
             *
             * @param keyCode კლავიშის კოდი
             *
             */
            CanvasInputsManager.prototype.keyWasDown = function (keyCode) {
                return this.keysPreviousFrameState[keyCode];
            };
            /**
             *
             * აბრუნებს კლავიშის მდგომარეობას მოცემულ კადრში.
             *
             * @param keyCode კლავიშის კოდი
             *
             *  */
            CanvasInputsManager.prototype.keyIsDown = function (keyCode) {
                return this.keysWhichAreDown[keyCode];
            };
            CanvasInputsManager.prototype.handleMouseOrTouchMove = function (ev) {
                if (ev instanceof MouseEvent) {
                    this.realTimeMouseXY.x = ev.offsetX;
                    this.realTimeMouseXY.y = ev.offsetY;
                    this.mouseCurrentScreenXY.x = ev.screenX;
                    this.mouseCurrentScreenXY.y = ev.screenY;
                    if (this.mousePreviousScreenXY.x == 0) {
                        this.mousePreviousScreenXY.x = this.mouseCurrentScreenXY.x;
                        this.mousePreviousScreenXY.y = this.mouseCurrentScreenXY.y;
                    }
                    this.mouseDeltaXYAccumulator.x += this.mouseCurrentScreenXY.x - this.mousePreviousScreenXY.x;
                    this.mouseDeltaXYAccumulator.y += this.mouseCurrentScreenXY.y - this.mousePreviousScreenXY.y;
                    this.mousePreviousScreenXY.x = this.mouseCurrentScreenXY.x;
                    this.mousePreviousScreenXY.y = this.mouseCurrentScreenXY.y;
                }
                if (ev instanceof TouchEvent) {
                    this.realTimeMouseXY.x = ev.touches[0].clientX; // ev.offsetX
                    this.realTimeMouseXY.y = ev.touches[0].clientY; //ev.offsetY
                    this.mouseCurrentScreenXY.x = ev.touches[0].screenX; //ev.screenX
                    this.mouseCurrentScreenXY.y = ev.touches[0].screenY; //ev.screenY
                    if (this.mousePreviousScreenXY.x == 0) {
                        this.mousePreviousScreenXY.x = this.mouseCurrentScreenXY.x;
                        this.mousePreviousScreenXY.y = this.mouseCurrentScreenXY.y;
                    }
                    this.mouseDeltaXYAccumulator.x += this.mouseCurrentScreenXY.x - this.mousePreviousScreenXY.x;
                    this.mouseDeltaXYAccumulator.y += this.mouseCurrentScreenXY.y - this.mousePreviousScreenXY.y;
                    this.mousePreviousScreenXY.x = this.mouseCurrentScreenXY.x;
                    this.mousePreviousScreenXY.y = this.mouseCurrentScreenXY.y;
                }
            };
            CanvasInputsManager.prototype.handleMouseDownOrTouchStart = function (ev) {
                this.realTimeMouseIsDown = true;
                this.realTimeMouseIsUp = false;
                this.mouseDownRegisteredOnce = false;
                if (ev instanceof MouseEvent) {
                    this.mouseButonsBitMap = ev.buttons;
                    this.realTimeMouseXY.x = ev.offsetX;
                    this.realTimeMouseXY.y = ev.offsetY;
                }
                if (ev instanceof TouchEvent) {
                    this.mouseButonsBitMap = Inputs.MOUSE_BUTTONS.LEFT;
                    this.realTimeMouseXY.x = ev.touches[0].clientX; // ev.offsetX
                    this.realTimeMouseXY.y = ev.touches[0].clientY; //ev.offsetY                  
                }
            };
            CanvasInputsManager.prototype.handleMouseUpOrTouchEnd = function (ev) {
                /*
                Checking weather mouse was down, bacause it could be downed
                outside canvas boundries and upped on the canvas, this shell
                not register as click.
                */
                if (this.realTimeMouseIsDown && this.realTimeMouseClicked == false) {
                    this.realTimeMouseClicked = true;
                }
                /*
                Taking note that mouse is up, not falsing
                realTimeMouseIsDown until processInput
                */
                this.realTimeMouseIsUp = true;
                if (ev instanceof MouseEvent) {
                }
                if (ev instanceof TouchEvent) {
                }
            };
            CanvasInputsManager.prototype.startProcessingInputFor = function (canvas) {
                var _this = this;
                this.canvas = canvas;
                this.canvas.addEventListener("mousemove", function (ev) {
                    _this.handleMouseOrTouchMove(ev);
                });
                this.canvas.addEventListener("mousedown", function (ev) {
                    _this.handleMouseDownOrTouchStart(ev);
                });
                this.canvas.addEventListener("mouseup", function (ev) {
                    _this.handleMouseUpOrTouchEnd(ev);
                });
                this.canvas.addEventListener("touchstart", function (ev) {
                    _this.handleMouseDownOrTouchStart(ev);
                });
                this.canvas.addEventListener("touchmove", function (ev) {
                    _this.handleMouseOrTouchMove(ev);
                });
                this.canvas.addEventListener("touchend", function (ev) {
                    _this.handleMouseUpOrTouchEnd(ev);
                });
                document.body.addEventListener("keydown", function (ev) {
                    _this.keysWhichAreDown[ev.keyCode] = true;
                });
                document.body.addEventListener("keyup", function (ev) {
                    _this.keysWhichAreDown[ev.keyCode] = false;
                });
            };
            /**
             *
             * უნდა გამოიძახო კადრის სულ ბოლოს.
             * რენდერის და ლოგიკის კოდის შემდეგ.
             *
             */
            CanvasInputsManager.prototype.postProcessInput = function () {
                this.copyCurrentKeyStatesToPreviousFrameStates();
            };
            //shell be called after openTimeFrame and before game logic and rendering
            CanvasInputsManager.prototype.processInput = function () {
                if (this.mouseXY.x != this.realTimeMouseXY.x || this.mouseXY.y != this.realTimeMouseXY.y) {
                    this.mouseMoved = true;
                }
                else {
                    this.mouseMoved = false;
                }
                this.mouseXY.x = this.realTimeMouseXY.x;
                this.mouseXY.y = this.realTimeMouseXY.y;
                this.mouseDeltaXY.x = this.mouseDeltaXYAccumulator.x;
                this.mouseDeltaXY.y = this.mouseDeltaXYAccumulator.y;
                this.mouseUp = this.realTimeMouseClicked;
                this.mouseIsDown = this.realTimeMouseIsDown;
                //Make mouseDown == true only for one frame
                if (this.mouseDownRegisteredOnce == false
                    && this.realTimeMouseIsDown == true) {
                    this.mouseDown = this.realTimeMouseIsDown;
                    this.mouseDownRegisteredOnce = true;
                }
                else {
                    this.mouseDown = false;
                }
                this.realTimeMouseClicked = false;
                if (this.realTimeMouseIsUp) {
                    this.realTimeMouseIsDown = false;
                }
                this.mouseDeltaXYAccumulator.x = 0.0;
                this.mouseDeltaXYAccumulator.y = 0.0;
            };
            return CanvasInputsManager;
        })();
        Inputs.CanvasInputsManager = CanvasInputsManager;
        (function (KeyCodes) {
            KeyCodes[KeyCodes["backspace"] = 8] = "backspace";
            KeyCodes[KeyCodes["space"] = 32] = "space";
            KeyCodes[KeyCodes["tab"] = 9] = "tab";
            KeyCodes[KeyCodes["enter"] = 13] = "enter";
            KeyCodes[KeyCodes["shift"] = 16] = "shift";
            KeyCodes[KeyCodes["ctrl"] = 17] = "ctrl";
            KeyCodes[KeyCodes["alt"] = 18] = "alt";
            KeyCodes[KeyCodes["pause_or_break"] = 19] = "pause_or_break";
            KeyCodes[KeyCodes["caps_lock"] = 20] = "caps_lock";
            KeyCodes[KeyCodes["escape"] = 27] = "escape";
            KeyCodes[KeyCodes["page_up"] = 33] = "page_up";
            KeyCodes[KeyCodes["page_down"] = 34] = "page_down";
            KeyCodes[KeyCodes["end"] = 35] = "end";
            KeyCodes[KeyCodes["home"] = 36] = "home";
            KeyCodes[KeyCodes["left_arrow"] = 37] = "left_arrow";
            KeyCodes[KeyCodes["up_arrow"] = 38] = "up_arrow";
            KeyCodes[KeyCodes["right_arrow"] = 39] = "right_arrow";
            KeyCodes[KeyCodes["down_arrow"] = 40] = "down_arrow";
            KeyCodes[KeyCodes["insert"] = 45] = "insert";
            KeyCodes[KeyCodes["delete"] = 46] = "delete";
            KeyCodes[KeyCodes["_0"] = 48] = "_0";
            KeyCodes[KeyCodes["_1"] = 49] = "_1";
            KeyCodes[KeyCodes["_2"] = 50] = "_2";
            KeyCodes[KeyCodes["_3"] = 51] = "_3";
            KeyCodes[KeyCodes["_4"] = 52] = "_4";
            KeyCodes[KeyCodes["_5"] = 53] = "_5";
            KeyCodes[KeyCodes["_6"] = 54] = "_6";
            KeyCodes[KeyCodes["_7"] = 55] = "_7";
            KeyCodes[KeyCodes["_8"] = 56] = "_8";
            KeyCodes[KeyCodes["_9"] = 57] = "_9";
            KeyCodes[KeyCodes["a"] = 65] = "a";
            KeyCodes[KeyCodes["b"] = 66] = "b";
            KeyCodes[KeyCodes["c"] = 67] = "c";
            KeyCodes[KeyCodes["d"] = 68] = "d";
            KeyCodes[KeyCodes["e"] = 69] = "e";
            KeyCodes[KeyCodes["f"] = 70] = "f";
            KeyCodes[KeyCodes["g"] = 71] = "g";
            KeyCodes[KeyCodes["h"] = 72] = "h";
            KeyCodes[KeyCodes["i"] = 73] = "i";
            KeyCodes[KeyCodes["j"] = 74] = "j";
            KeyCodes[KeyCodes["k"] = 75] = "k";
            KeyCodes[KeyCodes["l"] = 76] = "l";
            KeyCodes[KeyCodes["m"] = 77] = "m";
            KeyCodes[KeyCodes["n"] = 78] = "n";
            KeyCodes[KeyCodes["o"] = 79] = "o";
            KeyCodes[KeyCodes["p"] = 80] = "p";
            KeyCodes[KeyCodes["q"] = 81] = "q";
            KeyCodes[KeyCodes["r"] = 82] = "r";
            KeyCodes[KeyCodes["s"] = 83] = "s";
            KeyCodes[KeyCodes["t"] = 84] = "t";
            KeyCodes[KeyCodes["u"] = 85] = "u";
            KeyCodes[KeyCodes["v"] = 86] = "v";
            KeyCodes[KeyCodes["w"] = 87] = "w";
            KeyCodes[KeyCodes["x"] = 88] = "x";
            KeyCodes[KeyCodes["y"] = 89] = "y";
            KeyCodes[KeyCodes["z"] = 90] = "z";
            KeyCodes[KeyCodes["left_window_key"] = 91] = "left_window_key";
            KeyCodes[KeyCodes["right_window_key"] = 92] = "right_window_key";
            KeyCodes[KeyCodes["select_key"] = 93] = "select_key";
            KeyCodes[KeyCodes["numpad_0"] = 96] = "numpad_0";
            KeyCodes[KeyCodes["numpad_1"] = 97] = "numpad_1";
            KeyCodes[KeyCodes["numpad_2"] = 98] = "numpad_2";
            KeyCodes[KeyCodes["numpad_3"] = 99] = "numpad_3";
            KeyCodes[KeyCodes["numpad_4"] = 100] = "numpad_4";
            KeyCodes[KeyCodes["numpad_5"] = 101] = "numpad_5";
            KeyCodes[KeyCodes["numpad_6"] = 102] = "numpad_6";
            KeyCodes[KeyCodes["numpad_7"] = 103] = "numpad_7";
            KeyCodes[KeyCodes["numpad_8"] = 104] = "numpad_8";
            KeyCodes[KeyCodes["numpad_9"] = 105] = "numpad_9";
            KeyCodes[KeyCodes["multiply"] = 106] = "multiply";
            KeyCodes[KeyCodes["add"] = 107] = "add";
            KeyCodes[KeyCodes["subtract"] = 109] = "subtract";
            KeyCodes[KeyCodes["decimal_point"] = 110] = "decimal_point";
            KeyCodes[KeyCodes["divide"] = 111] = "divide";
            KeyCodes[KeyCodes["f1"] = 112] = "f1";
            KeyCodes[KeyCodes["f2"] = 113] = "f2";
            KeyCodes[KeyCodes["f3"] = 114] = "f3";
            KeyCodes[KeyCodes["f4"] = 115] = "f4";
            KeyCodes[KeyCodes["f5"] = 116] = "f5";
            KeyCodes[KeyCodes["f6"] = 117] = "f6";
            KeyCodes[KeyCodes["f7"] = 118] = "f7";
            KeyCodes[KeyCodes["f8"] = 119] = "f8";
            KeyCodes[KeyCodes["f9"] = 120] = "f9";
            KeyCodes[KeyCodes["f10"] = 121] = "f10";
            KeyCodes[KeyCodes["f11"] = 122] = "f11";
            KeyCodes[KeyCodes["f12"] = 123] = "f12";
            KeyCodes[KeyCodes["num_lock"] = 144] = "num_lock";
            KeyCodes[KeyCodes["scroll_lock"] = 145] = "scroll_lock";
            KeyCodes[KeyCodes["semi_colon"] = 186] = "semi_colon";
            KeyCodes[KeyCodes["equal_sign"] = 187] = "equal_sign";
            KeyCodes[KeyCodes["comma"] = 188] = "comma";
            KeyCodes[KeyCodes["dash"] = 189] = "dash";
            KeyCodes[KeyCodes["period"] = 190] = "period";
            KeyCodes[KeyCodes["forward_slash"] = 191] = "forward_slash";
            KeyCodes[KeyCodes["grave_accent"] = 192] = "grave_accent";
            KeyCodes[KeyCodes["open_bracket"] = 219] = "open_bracket";
            KeyCodes[KeyCodes["back_slash"] = 220] = "back_slash";
            KeyCodes[KeyCodes["close_braket"] = 221] = "close_braket";
            KeyCodes[KeyCodes["single_quote"] = 222] = "single_quote";
        })(Inputs.KeyCodes || (Inputs.KeyCodes = {}));
        var KeyCodes = Inputs.KeyCodes;
    })(Inputs = Ivane.Inputs || (Ivane.Inputs = {}));
})(Ivane || (Ivane = {}));
/*
 * Author Ivane Gegia http://ivane.info
 */
var Ivane;
(function (Ivane) {
    var Time;
    (function (Time) {
        /**
         * კლასი გათვლილია requestAnimationFrame-ებს შორის დროის დელტას სათვლელად
         * იყენებს მაღალი სიზუსტის ტაიმერს როცა ასეთი ხელმისაწვდომია.
         */
        var DeltaTimeComputer = (function () {
            function DeltaTimeComputer() {
                this.date = window.performance ? window.performance.now() : new Date().getDate;
                this.now = 0;
                this.time = 0;
                this.dt = 0;
                this.getDeltaTimeInSeconds = function () {
                    if (window.performance) {
                        this.now = window.performance.now();
                    }
                    else {
                        this.now = new Date().getTime();
                    }
                    this.dt = this.now - (this.time || this.now);
                    this.time = this.now;
                    return this.dt / 1000.0;
                };
                this.getDeltaTimeInSeconds();
            }
            return DeltaTimeComputer;
        })();
        Time.DeltaTimeComputer = DeltaTimeComputer;
    })(Time = Ivane.Time || (Ivane.Time = {}));
})(Ivane || (Ivane = {}));
/*
 * Author Ivane Gegia http://ivane.info
 */
var Ivane;
(function (Ivane) {
    var Utils;
    (function (Utils) {
        /**
         * კლასი გამოსადეგია დელეგატის შესაბუთათ, რომელიც gameStep-ში შეიძლება შესრულდეს.
         * და შესრულების შემდეგ გვინდა რომ მიუხედავად გამომძახებელი კოდის კვლავ შესრულებისა
         * შეფუთული ფუნქცია არ შესრულდეს სანამ არ გამოიძახება reset მეთოდი.
         */
        var FireOnce = (function () {
            function FireOnce(callback) {
                this.callbackCalled = false;
                this.callback = null;
                this.callback = callback;
            }
            /**
             * შეასრულოს შეფუთული დელეგატი.
             */
            FireOnce.prototype.fire = function () {
                if (this.callbackCalled === false && this.callback != null) {
                    this.callback();
                    this.callbackCalled = true;
                }
            };
            FireOnce.prototype.reset = function () {
                this.callbackCalled = false;
            };
            return FireOnce;
        })();
        Utils.FireOnce = FireOnce;
    })(Utils = Ivane.Utils || (Ivane.Utils = {}));
})(Ivane || (Ivane = {}));
/*
 * Author Ivane Gegia http://ivane.info
 */
var Ivane;
(function (Ivane) {
    var DOMHelpers;
    (function (DOMHelpers) {
        /**
         * მითითებული css კლასის მქონე ელემენტებზე აუქმებს მაუსით მონიშვნის ფუნქციონალს, გამოსადეგია UI ელემენტებისათვის.
         */
        function makeHTMLElementsNotSelectableByClassName(className) {
            var notSelectables = document.getElementsByClassName(className);
            for (var notSelectableIndex = 0; notSelectableIndex < notSelectables.length; notSelectableIndex++) {
                var notSelectable = notSelectables[notSelectableIndex];
                notSelectable.style["-moz-user-select"] = "none";
                notSelectable.style["-webkit-user-select"] = "none";
                notSelectable.style["-ms-user-select"] = "none";
                notSelectable.style["user-select"] = "none";
                notSelectable.style["-o-user-select"] = "none";
                notSelectable.style["cursor"] = "default";
                notSelectable.setAttribute("unselectable", "on");
                notSelectable.setAttribute("onselectstart", "return false");
                notSelectable.setAttribute("onmousedown", "return false");
            }
        }
        DOMHelpers.makeHTMLElementsNotSelectableByClassName = makeHTMLElementsNotSelectableByClassName;
    })(DOMHelpers = Ivane.DOMHelpers || (Ivane.DOMHelpers = {}));
})(Ivane || (Ivane = {}));
/*
 * Author Ivane Gegia http://ivane.info
 */
/// <reference path="DeltaTime.ts" />
var Ivane;
(function (Ivane) {
    var DOMHelpers;
    (function (DOMHelpers) {
        (function (POSITION_MODES) {
            POSITION_MODES[POSITION_MODES["RELATIVE"] = 0] = "RELATIVE";
            POSITION_MODES[POSITION_MODES["ABSOLUTE"] = 1] = "ABSOLUTE";
            POSITION_MODES[POSITION_MODES["FIXED"] = 2] = "FIXED";
        })(DOMHelpers.POSITION_MODES || (DOMHelpers.POSITION_MODES = {}));
        var POSITION_MODES = DOMHelpers.POSITION_MODES;
        function convertPositionModeToString(positionMode) {
            var positionModeString = "";
            switch (positionMode) {
                case POSITION_MODES.ABSOLUTE:
                    positionModeString = "absolute";
                    break;
                case POSITION_MODES.FIXED:
                    positionModeString = "fixed";
                    break;
                case POSITION_MODES.RELATIVE:
                    positionModeString = "relative";
                    break;
            }
            return positionModeString;
        }
        (function (DISPLAY_MODES) {
            DISPLAY_MODES[DISPLAY_MODES["BLOCK"] = 0] = "BLOCK";
            DISPLAY_MODES[DISPLAY_MODES["INLINE"] = 1] = "INLINE";
            DISPLAY_MODES[DISPLAY_MODES["NONE"] = 2] = "NONE";
        })(DOMHelpers.DISPLAY_MODES || (DOMHelpers.DISPLAY_MODES = {}));
        var DISPLAY_MODES = DOMHelpers.DISPLAY_MODES;
        function covertDisplayModeToString(displayMode) {
            var displayModeString = "";
            switch (displayMode) {
                case DISPLAY_MODES.BLOCK:
                    displayModeString = "block";
                    break;
                case DISPLAY_MODES.INLINE:
                    displayModeString = "inline";
                    break;
                case DISPLAY_MODES.NONE:
                    displayModeString = "none";
                    break;
            }
            return displayModeString;
        }
        /**
         * DOM-ის ელემენტების TypeScript შეფუთვები
         */
        var EXTHTMLElement = (function () {
            function EXTHTMLElement() {
            }
            EXTHTMLElement.createEXTDiv = function () {
                var div = document.createElement("div");
                var extHTMLElement = new EXTHTMLElement();
                extHTMLElement.domElement = div;
                return extHTMLElement;
            };
            EXTHTMLElement.prototype.getDOMElement = function () {
                return this.domElement;
            };
            EXTHTMLElement.prototype.setDisplayMode = function (displayMode) {
                this.domElement.style.display = covertDisplayModeToString(displayMode);
                return this;
            };
            EXTHTMLElement.prototype.setPositionMode = function (positionMode) {
                this.domElement.style.position = convertPositionModeToString(positionMode);
                return this;
            };
            EXTHTMLElement.prototype.setLeftInPixels = function (pixels) {
                this.domElement.style.left = pixels + "px";
                return this;
            };
            EXTHTMLElement.prototype.setTopInPixels = function (pixels) {
                this.domElement.style.top = pixels + "px";
                return this;
            };
            EXTHTMLElement.prototype.setRightInPixels = function (pixels) {
                this.domElement.style.right = pixels + "px";
                return this;
            };
            EXTHTMLElement.prototype.setBottomInPixels = function (pixels) {
                this.domElement.style.bottom = pixels + "px";
                return this;
            };
            EXTHTMLElement.prototype.setBackgroundColor = function (color) {
                this.domElement.style.backgroundColor = "#" + color.toString(16);
                return this;
            };
            EXTHTMLElement.prototype.getBackgroundColor = function () {
                return parseInt(this.domElement.style.backgroundColor.replace("#", ""), 16);
            };
            EXTHTMLElement.prototype.setWidthInPixels = function (width) {
                this.domElement.style.width = width + "px";
                return this;
            };
            EXTHTMLElement.prototype.getWidth = function () {
                return parseInt(this.domElement.style.width);
            };
            EXTHTMLElement.prototype.setHeightInPixels = function (height) {
                this.domElement.style.height = height + "px";
                return this;
            };
            EXTHTMLElement.prototype.getHeight = function () {
                return parseInt(this.domElement.style.height);
            };
            EXTHTMLElement.prototype.setZRotaion = function (rotationDegrees) {
                this.domElement.style.transform = "rotate(" + rotationDegrees + "deg)";
                return this;
            };
            EXTHTMLElement.prototype.setZIndex = function (zIndex) {
                this.domElement.style.zIndex = zIndex.toString();
                return this;
            };
            return EXTHTMLElement;
        })();
        DOMHelpers.EXTHTMLElement = EXTHTMLElement;
    })(DOMHelpers = Ivane.DOMHelpers || (Ivane.DOMHelpers = {}));
})(Ivane || (Ivane = {}));
/*
 * Author Ivane Gegia http://ivane.info
 */
var Ivane;
(function (Ivane) {
    var Utils;
    (function (Utils) {
        /**
         * ქმნის გადაცემული ტიპის ობიექტების მასივს.
         *
         * @param arrayObjectClass ტიპი რითიც უნდა შეივსოს მასივი
         * @param itemsCount მასივის ელემენტების რაოდენობა
         */
        function createArray(arrayObjectClass, itemsCount) {
            var arr = new Array();
            for (var arrayIndex = 0; arrayIndex < itemsCount; arrayIndex++) {
                arr[arrayIndex] = new arrayObjectClass();
            }
            return arr;
        }
        Utils.createArray = createArray;
    })(Utils = Ivane.Utils || (Ivane.Utils = {}));
})(Ivane || (Ivane = {}));
/*
 * Author Ivane Gegia http://ivane.info
 */
/// <reference path="../../definitions/gl-matrix/gl-matrix.d.ts" />   
/// <reference path="../Types.ts" />
var Ivane;
(function (Ivane) {
    var Math1;
    (function (Math1) {
        /**
          * კლასი განკუთვნილია N ხარისხის ბეზიერ წირის წერტილების სათვლელად.
          *
          */
        var BezierComputer = (function () {
            function BezierComputer(controlPointsBufferSize) {
                this.insertedContrPointsCount = 0;
                //ველები არის computeLength-ის მიერ გამოყენებისთვის
                this.t = 0;
                this.prevBezierPoint = vec3.create();
                this.length = 0;
                this.tangentVector = vec3.create();
                //computeTangentVectorAt გამოყენებისთვის
                this.tangentT = 0;
                this.initControlPointsBuffer(controlPointsBufferSize);
                this.bezierPoint = vec3.create();
            }
            BezierComputer.prototype.initControlPointsBuffer = function (pointsNumber) {
                this.controlPoints = new Array(pointsNumber);
                this.controlPoints_2 = new Array(pointsNumber);
                for (var pointIndex = 0; pointIndex < pointsNumber; pointIndex++) {
                    this.controlPoints[pointIndex] = vec3.create();
                    this.controlPoints_2[pointIndex] = vec3.create();
                }
            };
            /**
             *
             * ამატებს საკონტროლო წერტილს ბუფერში.
             * იღებს როგორც 2 ელემენტიან ისე 3 ელემენტიან.
             *
             */
            BezierComputer.prototype.pushControlPoint = function (x, y, z) {
                vec3.set(this.controlPoints[this.insertedContrPointsCount], x, y, z ? z : 0.0);
                this.insertedContrPointsCount++;
            };
            BezierComputer.prototype.copyControlPointsToBuffer2 = function () {
                for (var controlPoint = 0; controlPoint < this.insertedContrPointsCount; controlPoint++) {
                    vec3.copy(this.controlPoints_2[controlPoint], this.controlPoints[controlPoint]);
                }
            };
            /**
             *
             * თვლის ბეზიეს წერტილს მითითებული t-თვის,
             * ბეზიეს მრუდის ხარისხი უდრის საკონტროლო წერტილების ოდენობას.
             *
             * @return vec3 სამ ელემენტიან ვექტორს
             */
            BezierComputer.prototype.computeBezierPoint = function (t) {
                if (this.insertedContrPointsCount == 1) {
                    vec3.copy(this.bezierPoint, this.controlPoints[0]);
                    return this.bezierPoint;
                }
                this.copyControlPointsToBuffer2();
                // დათვლილი საკონტროლო წერტილების რაოდენობა
                // ეს რიცხვი  
                var contrPointsComputed = this.insertedContrPointsCount;
                while (contrPointsComputed > 1) {
                    var contrPointsComputed_2 = 0;
                    for (var controlPointIndex = 0; controlPointIndex < contrPointsComputed - 1; controlPointIndex++) {
                        vec3.lerp(this.controlPoints_2[controlPointIndex], this.controlPoints_2[controlPointIndex], this.controlPoints_2[controlPointIndex + 1], t);
                        contrPointsComputed_2++;
                    }
                    contrPointsComputed = contrPointsComputed_2;
                }
                vec3.copy(this.bezierPoint, this.controlPoints_2[0]);
                return this.bezierPoint;
            };
            /**
             * მეთოდი თვლის ბეზიეს მრუდის სიგრძეს მითითებული ბიჯის სიზუსტით.
             *
             * @param deltaT t ბიჯის ზომა რომესტაც მეთოდი გამოიყენებს მრუდის წერტილების გადასასთვლელად რომელთა შორის მანძილებსაც აჯამავს.
             */
            BezierComputer.prototype.computeLength = function (deltaT) {
                this.t = 0;
                this.length = 0;
                //ვითვლი პირველ 0 წერტილს და ვინახავ წინა წერტილის ველში
                this.computeBezierPoint(this.t);
                vec3.copy(this.prevBezierPoint, this.bezierPoint);
                while (true) {
                    this.t = this.t + deltaT;
                    if (this.t > 1) {
                        this.t = 1;
                    }
                    this.computeBezierPoint(this.t);
                    //მანძილს წინა წერტილსა და მიმდინარეს შორის ვუმატებ ჯამური სიგრძის შემნახველ ველს
                    this.length = this.length + vec3.distance(this.prevBezierPoint, this.bezierPoint);
                    vec3.copy(this.prevBezierPoint, this.bezierPoint);
                    if (this.t == 1) {
                        break;
                    }
                }
                return this.length;
            };
            /**
             * თვლის t წერტილში მხების ვექტორს.
             *
             * @param t
             * @param tDelta t-ს delta მიდამო, t-tDelta და t+tDelta რომელზეც აიგება მხები.
             */
            BezierComputer.prototype.computeTangentVectorAt = function (t, tDelta) {
                this.tangentT = t - tDelta;
                if (this.tangentT < 0)
                    this.tangentT = 0;
                this.computeBezierPoint(this.tangentT);
                vec3.copy(this.prevBezierPoint, this.bezierPoint);
                this.tangentT = t + tDelta;
                if (this.tangentT > 1)
                    this.tangentT = 1;
                this.computeBezierPoint(this.tangentT);
                vec3.subtract(this.tangentVector, this.bezierPoint, this.prevBezierPoint);
                return this.tangentVector;
            };
            BezierComputer.prototype.reset = function () {
                this.insertedContrPointsCount = 0;
            };
            return BezierComputer;
        })();
        Math1.BezierComputer = BezierComputer;
    })(Math1 = Ivane.Math1 || (Ivane.Math1 = {}));
})(Ivane || (Ivane = {}));
/*
 * Author Ivane Gegia http://ivane.info
 */
/// <reference path="Math1/BezierComputer.ts" />
var Ivane;
(function (Ivane) {
    var Math1;
    (function (Math1) {
        function random(min, max) {
            return Math.floor(Math.random() * (max - min + 1) + min);
        }
        Math1.random = random;
        var DEGREES = 180 / Math.PI;
        function radians2degrees(radians) {
            return radians * DEGREES;
        }
        Math1.radians2degrees = radians2degrees;
        function degrees2radians(degrees) {
            return degrees / DEGREES;
        }
        Math1.degrees2radians = degrees2radians;
    })(Math1 = Ivane.Math1 || (Ivane.Math1 = {}));
})(Ivane || (Ivane = {}));
/*
 * Author Ivane Gegia http://ivane.info
 *
 */
///<reference path="../../definitions/threejs/three.d.ts"/>
///<reference path="../../definitions/threejs/ivane_three.d.ts"/>
var Ivane;
(function (Ivane) {
    var ThreeJSHelpers;
    (function (ThreeJSHelpers) {
        function setUVsForRectangleMesh(rectangleMesh_in_out, topLeftU, topLeftV, topRightU, topRightV, bottomRightU, bottomRightV, bottomLeftU, bottomLeftV) {
            setUVsForRectangleGeometry(rectangleMesh_in_out.geometry, topLeftU, topLeftV, topRightU, topRightV, bottomRightU, bottomRightV, bottomLeftU, bottomLeftV);
        }
        ThreeJSHelpers.setUVsForRectangleMesh = setUVsForRectangleMesh;
        function setUVsForRectangleGeometry(rectangleGeometry_in_out, topLeftU, topLeftV, topRightU, topRightV, bottomRightU, bottomRightV, bottomLeftU, bottomLeftV) {
            rectangleGeometry_in_out.faceVertexUvs[0][0][0].set(bottomRightU, bottomRightV);
            rectangleGeometry_in_out.faceVertexUvs[0][0][1].set(topRightU, topRightV);
            rectangleGeometry_in_out.faceVertexUvs[0][0][2].set(topLeftU, topLeftV);
            rectangleGeometry_in_out.faceVertexUvs[0][1][0].set(bottomRightU, bottomRightV);
            rectangleGeometry_in_out.faceVertexUvs[0][1][1].set(topLeftU, topLeftV);
            rectangleGeometry_in_out.faceVertexUvs[0][1][2].set(bottomLeftU, bottomLeftV);
            rectangleGeometry_in_out.uvsNeedUpdate = true;
        }
        ThreeJSHelpers.setUVsForRectangleGeometry = setUVsForRectangleGeometry;
        function getOrtho2DCoordinatesFromPixelCoordinates(viewWidthInPixels, viewHeightInPixels, pointerTopLeftXInPixels, pointerTopLeftYInPixels, orthoCamera, ortho2DCoordiantes__out) {
            var orthoRangeOfX = -orthoCamera.left + orthoCamera.right;
            var orthoRangeOfY = -orthoCamera.bottom + orthoCamera.top;
            var x = orthoRangeOfX * (pointerTopLeftXInPixels / viewWidthInPixels) + orthoCamera.left;
            var y = orthoRangeOfY * (1 - pointerTopLeftYInPixels / viewHeightInPixels) + orthoCamera.bottom;
            ortho2DCoordiantes__out.set(x, y);
        }
        ThreeJSHelpers.getOrtho2DCoordinatesFromPixelCoordinates = getOrtho2DCoordinatesFromPixelCoordinates;
        function orthoViewCoordinateToWorld(viewCoodinate, orthoCamera, worldCoordinate_out) {
            worldCoordinate_out.x = orthoCamera.position.x + viewCoodinate.x;
            worldCoordinate_out.y = orthoCamera.position.y + viewCoodinate.y;
            worldCoordinate_out.z = orthoCamera.position.z;
        }
        ThreeJSHelpers.orthoViewCoordinateToWorld = orthoViewCoordinateToWorld;
        function addGrid(scene) {
            var verticalGridLineGeometry = new THREE.CubeGeometry(.025, 20, 0.001);
            var horizontalGridLineGeometry = new THREE.CubeGeometry(20, .025, 0.001);
            var gridLineMaterial = new THREE.MeshBasicMaterial({
                color: 0xbcbcbc
            });
            var greenLineMaterial = new THREE.MeshBasicMaterial({
                color: 0x00ff00
            });
            var redLineMaterial = new THREE.MeshBasicMaterial({
                color: 0xff0000
            });
            for (var x = 0; x < 20; x++) {
                var verticalGridLineMesh = new THREE.Mesh(verticalGridLineGeometry, gridLineMaterial);
                scene.add(verticalGridLineMesh);
                if (x == 10) {
                    verticalGridLineMesh.scale.set(2, 10, 0.001);
                    verticalGridLineMesh.material = greenLineMaterial;
                }
                verticalGridLineMesh.scale.set(2, 10, 0.001);
                verticalGridLineMesh.position.set(-10 + x, 0, 0);
            }
            for (var y = 0; y < 20; y++) {
                var horizontalGridLineMesh = new THREE.Mesh(horizontalGridLineGeometry, gridLineMaterial);
                scene.add(horizontalGridLineMesh);
                if (y == 10) {
                    horizontalGridLineMesh.scale.set(1, 2, 0.001);
                    horizontalGridLineMesh.material = redLineMaterial;
                }
                horizontalGridLineMesh.scale.set(1, 2, 0.001);
                horizontalGridLineMesh.position.set(0, -10 + y, -0.1);
            }
        }
        ThreeJSHelpers.addGrid = addGrid;
        function createRectangleGeometry(width, height) {
            var geometry = new THREE.Geometry();
            var halfWidth = width / 2;
            var halfHeight = height / 2;
            //geometry.vertices.push(new )
            geometry.vertices.push(new THREE.Vector3(-halfWidth, halfHeight, 0));
            geometry.vertices.push(new THREE.Vector3(halfWidth, halfHeight, 0));
            geometry.vertices.push(new THREE.Vector3(halfWidth, -halfHeight, 0));
            geometry.vertices.push(new THREE.Vector3(-halfWidth, -halfHeight, 0));
            geometry.faces.push(new THREE.Face3(2, 1, 0));
            geometry.faces.push(new THREE.Face3(2, 0, 3));
            geometry.faceVertexUvs[0].push([
                new THREE.Vector2(1, 0),
                new THREE.Vector2(1, 1),
                new THREE.Vector2(0, 1)
            ], [
                new THREE.Vector2(1, 0),
                new THREE.Vector2(0, 1),
                new THREE.Vector2(0, 0)
            ]);
            geometry.uvsNeedUpdate = true;
            geometry.computeFaceNormals();
            geometry.computeVertexNormals();
            return geometry;
        }
        ThreeJSHelpers.createRectangleGeometry = createRectangleGeometry;
        function createRectangleMesh(width, height, material_nullable) {
            var geom = createRectangleGeometry(width, height);
            var rectangleMeshMaterial = material_nullable;
            if (rectangleMeshMaterial == null) {
                rectangleMeshMaterial = new THREE.MeshBasicMaterial({
                    color: 0xffff00
                });
            }
            var rectangleMesh = new THREE.Mesh(geom, rectangleMeshMaterial);
            return rectangleMesh;
        }
        ThreeJSHelpers.createRectangleMesh = createRectangleMesh;
        function createCircleMesh(radius, segments, material_nullable) {
            var geom = new THREE.CircleGeometry(radius, segments);
            var rectangleMeshMaterial = material_nullable;
            if (rectangleMeshMaterial == null) {
                rectangleMeshMaterial = new THREE.MeshBasicMaterial({
                    color: 0xffff00
                });
            }
            var rectangleMesh = new THREE.Mesh(geom, rectangleMeshMaterial);
            return rectangleMesh;
        }
        ThreeJSHelpers.createCircleMesh = createCircleMesh;
        function loadOBJFromWeb(url, onComplete, onProgress, onFail) {
            var loadManager = new THREE.LoadingManager();
            var objLoader = new THREE.OBJLoader(loadManager);
            objLoader.load(url, _onComplete, _onProgress, _onFail);
            function _onComplete(object) {
                onComplete(object);
            }
            function _onProgress(xhr) {
                if (xhr.lengthComputable
                    && (onProgress !== undefined && onProgress != null)) {
                    onProgress(xhr);
                }
            }
            function _onFail(xhr) {
                if (onFail !== undefined) {
                    onFail();
                }
            }
        }
        ThreeJSHelpers.loadOBJFromWeb = loadOBJFromWeb;
    })(ThreeJSHelpers = Ivane.ThreeJSHelpers || (Ivane.ThreeJSHelpers = {}));
})(Ivane || (Ivane = {}));
/*
 * Author Ivane Gegia http://ivane.info
 */
///<reference path="../definitions/threejs/three.d.ts" />
///<reference path="DeltaTime.ts"/>
///<reference path="CanvasInputsManager.ts"/>
///<reference path="ThreeJSHelpers/ThreeJSHelpers.ts"/>
var Ivane;
(function (Ivane) {
    var ThreeJSHelpers;
    (function (ThreeJSHelpers) {
        var GameClassThreeJS = (function () {
            function GameClassThreeJS() {
                this.mouseXYMainOrthoCameraView = new THREE.Vector2(0.0, 0.0);
                this.mouseXYMainOrthoCameraWorld = new THREE.Vector3(0.0, 0.0, 0.0);
                this.enableMiddleMouseCameraDrag = false;
                this.initWithOrthoCameraCalled = false;
                this.disableDefaultRenderer = false;
            }
            GameClassThreeJS.prototype.setEnableMiddleMouseCameraDrag = function (enable) {
                this.enableMiddleMouseCameraDrag = enable;
            };
            GameClassThreeJS.prototype.setDisableDefaultRenderer = function (disable) {
                this.disableDefaultRenderer = disable;
            };
            GameClassThreeJS.prototype.initWithOrthoCamera = function (cameraSettings, rendererSettings, rendererContainer) {
                this.scene = new THREE.Scene();
                var viewRatio = rendererSettings.viewWidth / rendererSettings.viewHeight;
                this.mainOrthoCamera = new THREE.OrthographicCamera(-cameraSettings.height * viewRatio, cameraSettings.height * viewRatio, cameraSettings.height, -cameraSettings.height, cameraSettings.near, cameraSettings.far);
                this.scene.add(this.mainOrthoCamera);
                this.mainOrthoCamera.position.set(0, 0, 10);
                this.renderer = new THREE.WebGLRenderer();
                this.renderer.setClearColor(rendererSettings.clearColor);
                this.renderer.setSize(rendererSettings.viewWidth, rendererSettings.viewHeight);
                rendererContainer.appendChild(this.renderer.domElement);
                this.deltaTimeComputer = new Ivane.Time.DeltaTimeComputer();
                this.deltaTime = this.deltaTimeComputer.getDeltaTimeInSeconds();
                this.inputsManager = new Ivane.Inputs.CanvasInputsManager();
                this.inputsManager.startProcessingInputFor(this.renderer.domElement);
                this.initWithOrthoCameraCalled = true;
            };
            GameClassThreeJS.prototype.getMouseCameraCoordinatesInOrthoCamera = function (mouseXY_out) {
                Ivane.ThreeJSHelpers.getOrtho2DCoordinatesFromPixelCoordinates(this.renderer.domElement.width, this.renderer.domElement.height, this.inputsManager.mouseXY.x, this.inputsManager.mouseXY.y, this.mainOrthoCamera, mouseXY_out);
            };
            GameClassThreeJS.prototype.run = function () {
                if (this.initWithOrthoCameraCalled == false) {
                    console.error("GameClassThreeJS.initWithOrthoCamera was not called!");
                    return;
                }
                this.animationFrameFunction();
            };
            GameClassThreeJS.prototype.defaultRender = function () {
                if (this.mainOrthoCamera) {
                    this.renderer.render(this.scene, this.mainOrthoCamera);
                }
            };
            GameClassThreeJS.prototype.animationFrameFunction = function () {
                var _this = this;
                this.deltaTime = this.deltaTimeComputer.getDeltaTimeInSeconds();
                this.inputsManager.processInput();
                this.getMouseCameraCoordinatesInOrthoCamera(this.mouseXYMainOrthoCameraView);
                this.mouseXYMainOrthoCameraWorld.set(this.mouseXYMainOrthoCameraView.x + this.mainOrthoCamera.position.x, this.mouseXYMainOrthoCameraView.y + this.mainOrthoCamera.position.y, this.mainOrthoCamera.position.z);
                this.dragOrthoCameraWithMouseMIddleButtonIfDragEnabled();
                this.gameStep();
                if (this.disableDefaultRenderer == false) {
                    this.defaultRender();
                }
                this.inputsManager.postProcessInput();
                requestAnimationFrame(function () {
                    _this.animationFrameFunction();
                });
            };
            GameClassThreeJS.prototype.dragOrthoCameraWithMouseMIddleButtonIfDragEnabled = function () {
                if (this.enableMiddleMouseCameraDrag
                    && this.inputsManager.mouseIsDown
                    && this.inputsManager.mouseButonsBitMap & Ivane.Inputs.MOUSE_BUTTONS.MIDDLE) {
                    var pixelToUnitsRatio = this.mainOrthoCamera.top * 2 / this.renderer.domElement.height;
                    this.mainOrthoCamera.position.x -= this.inputsManager.mouseDeltaXY.x * pixelToUnitsRatio;
                    this.mainOrthoCamera.position.y += this.inputsManager.mouseDeltaXY.y * pixelToUnitsRatio;
                }
            };
            GameClassThreeJS.prototype.gameStep = function () {
                throw new Ivane.Exceptions.NotImplemetedException();
            };
            return GameClassThreeJS;
        })();
        ThreeJSHelpers.GameClassThreeJS = GameClassThreeJS;
    })(ThreeJSHelpers = Ivane.ThreeJSHelpers || (Ivane.ThreeJSHelpers = {}));
})(Ivane || (Ivane = {}));
/*
 * Author Ivane Gegia http://ivane.info
 */
///<reference path="../definitions/threejs/three.d.ts" />
///<reference path="DeltaTime.ts"/>
///<reference path="CanvasInputsManager.ts"/>
///<reference path="ThreeJSHelpers/ThreeJSHelpers.ts"/>
var Ivane;
(function (Ivane) {
    var Canvas2DHelpers;
    (function (Canvas2DHelpers) {
        /**
         * წერტილის აღმწერი კლასი
         */
        var Point2D = (function () {
            function Point2D() {
                this.deltaX = 0.0;
                this.deltaY = 0.0;
                this.x = 0.0;
                this.y = 0.0;
            }
            Point2D.prototype.initWithPoint2D = function (point2D) {
                this.x = point2D.x;
                this.y = point2D.y;
            };
            Point2D.prototype.distance = function (tillPoint) {
                this.deltaX = tillPoint.x - this.x;
                this.deltaY = tillPoint.y - this.y;
                return Math.sqrt(this.deltaX * this.deltaX + this.deltaY * this.deltaY);
            };
            return Point2D;
        })();
        Canvas2DHelpers.Point2D = Point2D;
        /**
         * კლასი აღწერს მონაკვეთს რომელიც გავლებულია a,b წერტილებზე
         */
        var Line2D = (function () {
            function Line2D() {
                this.a = new Point2D();
                this.b = new Point2D();
            }
            return Line2D;
        })();
        Canvas2DHelpers.Line2D = Line2D;
        var GameClassCanvas2D = (function () {
            function GameClassCanvas2D() {
                this.enableMiddleMouseCameraDrag = false;
                this.initCalled = false;
                this.worldOffset = new Point2D();
                this.convertCenterPixelCoordinatesToWorldCoordinatesWithRespectToWorldOffset_Point2D = new Ivane.Canvas2DHelpers.Point2D();
                /**
                 * მაუსის კოორდინატები სამყაროში
                 */
                this.mouseXYWorld = new Point2D();
                /**
                 * კამერის წანაცვლება ხედის ცენტრიდან
                 */
                this.cameraOffsetInPixels = new Point2D();
                this.enableMiddleMouseButtonDrag = true;
                /**
                 * კოეფიციენტი, 1, სამყაროს ერთეულს რამდენი პიქსელი შეესაბამება
                 */
                this.pointToPixelForDragginWithMiddleMouseButton = 1;
            }
            GameClassCanvas2D.prototype.initCanvas2D = function (width, height, canvasContainer) {
                var viewRatio = width / height;
                this.canvasElement = document.createElement("canvas");
                this.canvasElement.width = width;
                this.canvasElement.height = height;
                canvasContainer.appendChild(this.canvasElement);
                this.canvas2DContext = this.canvasElement.getContext("2d");
                this.deltaTimeComputer = new Ivane.Time.DeltaTimeComputer();
                this.deltaTime = this.deltaTimeComputer.getDeltaTimeInSeconds();
                this.inputsManager = new Ivane.Inputs.CanvasInputsManager();
                this.inputsManager.startProcessingInputFor(this.canvasElement);
                this.initCalled = true;
                this.canvasCenterX = this.canvasElement.width / 2;
                this.canvasCenterY = this.canvasElement.height / 2;
            };
            GameClassCanvas2D.prototype.run = function () {
                if (this.initCalled == false) {
                    console.error("GameClassThreeJS.initWithOrthoCamera was not called!");
                    return;
                }
                this.animationFrameFunction();
            };
            /**
             * გადაყავს სამყაროს კოორდინატები ხედის პიქსელ კოორდინატებში, აბრუნებს Y ღერძს.
             * ითვალისწინებს worldOffset-ს
             *
             * @param worldX სამყაროს X კოორდინატი
             * @param worldY სამყაროს Y კოორდინატი
             */
            GameClassCanvas2D.prototype.convertPointFromWorldToPixelCoordinates = function (worldX, worldY, scale, pixelCoordinate_out, withWorldOffset) {
                if (withWorldOffset === true) {
                    pixelCoordinate_out.x = (worldX + this.worldOffset.x) * scale;
                    pixelCoordinate_out.y = (worldY + this.worldOffset.y) * scale * -1.0;
                }
                else {
                    pixelCoordinate_out.x = worldX * scale;
                    pixelCoordinate_out.y = worldY * scale * -1.0;
                }
            };
            /**
             * გადაყავს ხედის პიქსელ კოოდინატი სამყაროს კოორდიანტში, აბრუნებს Y ღერძს.
             * ითვალისწინებს worldOffset-ს
             *
             * @param viewPixelX პიქსელის კოოდრინატი Canvas2D-ში
             * @param viewPixelY პიქსელის კოორდინატი Canvas2D-ში
             * @param withWorldOffset გაითვალისწინოს თუ არა სამყაროს წანაცვლება ისე რომ ხედის კოორდინატი სწორ სამყაროს წერტილში გადაიყვანოს.
             */
            GameClassCanvas2D.prototype.convertViewPixelCoordinatesToWorldCoordiantes = function (viewPixelX, viewPixelY, pixelsPerWorldUnit, worldCoordinates_out, withWorldOffset) {
                //მასშტაბის გადათვლა
                worldCoordinates_out.x = viewPixelX * pixelsPerWorldUnit;
                worldCoordinates_out.y = viewPixelY * pixelsPerWorldUnit * -1;
                //როცა ხედის პიქსელური კოოდინატებიდან გადმოვგვყავს წანაცვლებული სამყაროს კოორდინატებში, წერტილი სამყაროს წანაცვლების საპირისპიროდ უნდა წანაცვლდეს რომ
                //კორექტული სამყაროსეული კოორდინატები გამოვიდეს. 
                if (withWorldOffset === true) {
                    worldCoordinates_out.x += (this.worldOffset.x * -1);
                    worldCoordinates_out.y += (this.worldOffset.y * -1);
                }
            };
            /**
             * გადაყავს პიქსელური კოორდინატები(პიქსელური ხედის ცენტრიდან ათვლილი), სამყაროს კოოდინატებში(სამყაროს წანაცვლების/"კამერის" გათვალისწინებით)
             *
             * @param worldToPixelsScale რამდენი პიქსელი შეესაბამება 1 სამყაროს ერთეულს
             */
            GameClassCanvas2D.prototype.convertCenterPixelCoordinatesToWorldCoordinatesWithRespectToWorldOffset = function (worldToPixelsScale) {
                // ვაკონვერტირებ მაუსის ეკრანულ კოორდინატებს, გასწორებულს ხედის ცენტრზე, 
                // სამყაროს კოორდინატებში რომელზეც შეიქმნება მანქანა.
                this.convertViewPixelCoordinatesToWorldCoordiantes(this.inputsManager.mouseXY.x - this.canvasElement.width / 2, this.inputsManager.mouseXY.y - this.canvasElement.height / 2, 1.0 / worldToPixelsScale, this.convertCenterPixelCoordinatesToWorldCoordinatesWithRespectToWorldOffset_Point2D, true);
                return this.convertCenterPixelCoordinatesToWorldCoordinatesWithRespectToWorldOffset_Point2D;
            };
            /**
             *
             * ანგარიშობს მაუსის კოორდიანტებს სამყაროს საკოორდინატო სივცეში
             *
             *
             */
            GameClassCanvas2D.prototype.convertMouseViewCoordinatesToWorld = function (worldToPixelsScale) {
                this.convertViewPixelCoordinatesToWorldCoordiantes(this.inputsManager.mouseXY.x - this.canvasElement.width / 2, this.inputsManager.mouseXY.y - this.canvasElement.height / 2, 1.0 / worldToPixelsScale, this.mouseXYWorld, true);
                return this.mouseXYWorld;
            };
            /**
             * Canvas2D კოორდინატთა სათავე გადააქვს ხედის ცენტრში.
             * მაგალიტად: თუ ხედის ზომებია 800x600 სამყაროს (0,0) წერტილი დაჯდება (400,300) ხედის წერტილზე.
             *
             */
            GameClassCanvas2D.prototype.centerOriginPointInCanvas = function () {
                this.canvas2DContext.save();
                this.canvas2DContext.translate(Math.ceil(this.canvasElement.width) / 2, Math.ceil(this.canvasElement.height / 2));
            };
            /**
             * აბრუნებს ცენტრზე გასწორებულ ხედს წინა მდგომარეობაში
             */
            GameClassCanvas2D.prototype.restoreOriginPoint = function () {
                this.canvas2DContext.restore();
            };
            GameClassCanvas2D.prototype.animationFrameFunction = function () {
                var _this = this;
                this.deltaTime = this.deltaTimeComputer.getDeltaTimeInSeconds();
                this.inputsManager.processInput();
                var shellRestoreTransform = false;
                this.gameStep();
                this.inputsManager.postProcessInput();
                requestAnimationFrame(function () {
                    _this.animationFrameFunction();
                });
            };
            GameClassCanvas2D.prototype.distance2D = function (x1, y1, x2, y2) {
                var deltaX = x2 - x1;
                var deltaY = y2 - y1;
                return Math.sqrt(deltaX * deltaX + deltaY * deltaY);
            };
            GameClassCanvas2D.prototype.dragCameraWithMiddleMouseButton = function (scale) {
                if (this.enableMiddleMouseButtonDrag
                    && this.inputsManager.mouseIsDown
                    && this.inputsManager.mouseButonsBitMap & Ivane.Inputs.MOUSE_BUTTONS.MIDDLE) {
                    this.worldOffset.x += (this.inputsManager.mouseDeltaXY.x / this.pointToPixelForDragginWithMiddleMouseButton) * scale;
                    this.worldOffset.y += (this.inputsManager.mouseDeltaXY.y / this.pointToPixelForDragginWithMiddleMouseButton * -1) * scale;
                }
            };
            GameClassCanvas2D.prototype.gameStep = function () {
                throw new Ivane.Exceptions.NotImplemetedException();
            };
            return GameClassCanvas2D;
        })();
        Canvas2DHelpers.GameClassCanvas2D = GameClassCanvas2D;
    })(Canvas2DHelpers = Ivane.Canvas2DHelpers || (Ivane.Canvas2DHelpers = {}));
})(Ivane || (Ivane = {}));
/*
 * Author Ivane Gegia http://ivane.info
 */
///<reference path="DeltaTime.ts"/>
///<reference path="CanvasInputsManager.ts"/>
/// <reference path="../definitions/gl-matrix/gl-matrix.d.ts" />
var Ivane;
(function (Ivane) {
    var GameClasses;
    (function (GameClasses) {
        var GameClassWebGL = (function () {
            function GameClassWebGL(width, height, canvasContainer, preserveDrawingBuffer) {
                this.viewRatio = 0.0;
                /**
                 * ინახავს მაუსის პიქსელურ კოორდინატებს
                 * გადაყვანილს webgl-ის ხედის კოორდინატებში
                 *
                 * (-1, 1)----------(1,1)
                 * |          |        |
                 * |          |        |
                 * |          |        |
                 * |---------(0,0)-----|
                 * |          |        |
                 * |          |        |
                 * |          |        |
                 * (-1,-1)---------(1,-1)
                 */
                this.mouseViewCoordiantes = vec3.create();
                /**
                 * მაუსის ხედის კოორდინატები, სიგანე-სიმაღლის შეფარდების გათვალისწინებით.
                 * ანუ მართკუთხა ხედის შემთხვევისთვის
                 * თუ მაგალითად ხედი არის განიერი, ამ შემთხვევაში x კოორდინატი გაცდება [-1,1] სეგმენტს
                 *
                 * (-1, 1)----------(1,1)
                 * |          |        |
                 * |          |        |  *(კოორდინატი, widescreen შემთხვევაში)
                 * |          |        |
                 * |---------(0,0)-----|
                 * |          |        |
                 * |          |        |
                 * |          |        |
                 * (-1,-1)---------(1,-1)
                 */
                this.mouseViewCoordiantesProportional = vec3.create();
                //მუასის პიქსელი კოორდინატების დროებითი შემნახველი ვექტორი	
                this.mousePixelCoordinatesCentered = vec3.create();
                this.canvasElement = document.createElement("canvas");
                this.canvasElement.width = width;
                this.canvasElement.height = height;
                canvasContainer.appendChild(this.canvasElement);
                //webgl კონტექსტის მოპოვება
                this.gl = this.canvasElement.getContext("webgl", {
                    preserveDrawingBuffer: preserveDrawingBuffer
                });
                //დროის delta-ს მთველელი ინიციალიზაცია
                this.deltaTimeComputer = new Ivane.Time.DeltaTimeComputer();
                this.deltaTime = this.deltaTimeComputer.getDeltaTimeInSeconds();
                //canvasElement-ზე inputsManager-ირ ინიციალიზაცია რომ ააგროვოს input-ი
                this.inputsManager = new Ivane.Inputs.CanvasInputsManager();
                this.inputsManager.startProcessingInputFor(this.canvasElement);
                this.updateViewRatioAndCanvasCenterXY(width, height);
            }
            GameClassWebGL.prototype.updateViewRatioAndCanvasCenterXY = function (width, height) {
                this.viewRatio = width / height;
                //canvas ელემენტის ცენტრის კოორდინატები pixel-ებში
                this.canvasCenterX = this.canvasElement.width / 2;
                this.canvasCenterY = this.canvasElement.height / 2;
            };
            /**
             * გადაყავს მაუსის ხედის პიქსეული კოორდინატები,
             * webgl_ის ხედის კოორდინატებში
             */
            GameClassWebGL.prototype.computeMouseViewCoordinates = function () {
                //ვაკოპირებ მაუსის პიქსეულ კოორდინატებს
                vec2.set(this.mousePixelCoordinatesCentered, this.inputsManager.mouseXY.x, this.inputsManager.mouseXY.y);
                //ვაკონვერტირებ კოორდინატებს ისე თითქოს ცენტრი ითველობდეს ხედის ცენტრიდან
                this.mousePixelCoordinatesCentered[0] = this.mousePixelCoordinatesCentered[0] - this.canvasCenterX;
                this.mousePixelCoordinatesCentered[1] = this.mousePixelCoordinatesCentered[1] - this.canvasCenterY;
                //ვატრიალებ y კოორდინატს
                this.mousePixelCoordinatesCentered[1] = -1.0 * this.mousePixelCoordinatesCentered[1];
                //პროპორციული კოორდინატების დათვლა, ანუ აქ ვითვალისწინებ სიგანე-სიმაღლის შეფარდებას
                this.mouseViewCoordiantesProportional[0] = this.mousePixelCoordinatesCentered[0] / this.canvasCenterX;
                this.mouseViewCoordiantesProportional[1] = this.mousePixelCoordinatesCentered[1] / this.canvasCenterY;
                this.mouseViewCoordiantesProportional[0] = this.mouseViewCoordiantesProportional[0] * this.viewRatio;
                //კოორდინატების გადაყვანა ერთეულოვან სივრცეში
                this.mouseViewCoordiantes[0] = this.mousePixelCoordinatesCentered[0] / this.canvasCenterX;
                this.mouseViewCoordiantes[1] = this.mousePixelCoordinatesCentered[1] / this.canvasCenterY;
            };
            /**
             * ფუნცია რომელის სტარტავს game loop-ს
             */
            GameClassWebGL.prototype.run = function () {
                this.animationFrameFunction();
            };
            GameClassWebGL.prototype.animationFrameFunction = function () {
                var _this = this;
                this.deltaTime = this.deltaTimeComputer.getDeltaTimeInSeconds();
                this.inputsManager.processInput();
                this.computeMouseViewCoordinates();
                this.gameStep();
                this.inputsManager.postProcessInput();
                requestAnimationFrame(function () {
                    _this.animationFrameFunction();
                });
            };
            GameClassWebGL.prototype.gameStep = function () {
                throw new Ivane.Exceptions.NotImplemetedException();
            };
            return GameClassWebGL;
        })();
        GameClasses.GameClassWebGL = GameClassWebGL;
    })(GameClasses = Ivane.GameClasses || (Ivane.GameClasses = {}));
})(Ivane || (Ivane = {}));
/*
 * Author Ivane Gegia http://ivane.info
 */
///<reference path="../definitions/threejs/three.d.ts" />
///<reference path="DeltaTime.ts"/>
///<reference path="CanvasInputsManager.ts"/>
///<reference path="ThreeJSHelpers/ThreeJSHelpers.ts"/>
/// <reference path="../definitions/gl-matrix/gl-matrix.d.ts" />
var Ivane;
(function (Ivane) {
    var GameClasses;
    (function (GameClasses) {
        /**
         * ზოგადი სათამაშო კლასი, რომელსაც აქვს მინიმალური ფუნქციონალი
         * არ აქვს გასაზღვრული გრაფიკული კონტექსტის ტიპი, 2d/3d
         */
        var GameClass = (function () {
            function GameClass() {
                this.enableMiddleMouseCameraDrag = false;
                this.initCalled = false;
                /**
                 * vec3
                 * 3 კომპონენტიანია ტექნიკური მიზეზებით
                 *
                 */
                this._mouseXYInGLView = vec3.create();
            }
            GameClass.prototype.initCanvas2D = function (width, height, canvasContainer) {
                var viewRatio = width / height;
                this.canvasElement = document.createElement("canvas");
                this.canvasElement.width = width;
                this.canvasElement.height = height;
                canvasContainer.appendChild(this.canvasElement);
                this.deltaTimeComputer = new Ivane.Time.DeltaTimeComputer();
                this.deltaTime = this.deltaTimeComputer.getDeltaTimeInSeconds();
                this.inputsManager = new Ivane.Inputs.CanvasInputsManager();
                this.inputsManager.startProcessingInputFor(this.canvasElement);
                this.initCalled = true;
                this.computeCanvasCenterXY();
            };
            GameClass.prototype.computeCanvasCenterXY = function () {
                this.canvasCenterX = this.canvasElement.width / 2;
                this.canvasCenterY = this.canvasElement.height / 2;
            };
            GameClass.prototype.run = function () {
                if (this.initCalled == false) {
                    console.error("GameClassThreeJS.initWithOrthoCamera was not called!");
                    return;
                }
                this.animationFrameFunction();
            };
            GameClass.prototype.animationFrameFunction = function () {
                var _this = this;
                this.deltaTime = this.deltaTimeComputer.getDeltaTimeInSeconds();
                this.inputsManager.processInput();
                var shellRestoreTransform = false;
                this.getMouseXYInGLView();
                this.gameStep();
                this.inputsManager.postProcessInput();
                requestAnimationFrame(function () {
                    _this.animationFrameFunction();
                });
            };
            /**
             * @return true თუ კლავიშას მიმდინარე კადრში დაეჭირა.
             */
            GameClass.prototype.keyDown = function (keyCode) {
                return this.inputsManager.keyWasDown(keyCode) == false && this.inputsManager.keyIsDown(keyCode);
            };
            /**
             * @return true თუ კლავიშას მიმდნარე კადრში აეშვა ხელი.
             */
            GameClass.prototype.keyUp = function (keyCode) {
                return this.inputsManager.keyWasDown(keyCode) == true && this.inputsManager.keyIsDown(keyCode);
            };
            /**
             * გადაყავს მაუსის პიქსეული კოორდინატები, gl ხედის კოორდინატებში.
             *
             * შედეგები ინახება _mouseXY-ში
             *
             * @return GLM.IArray
             *  */
            GameClass.prototype.getMouseXYInGLView = function (x, y) {
                this.computeCanvasCenterXY();
                if (x == undefined) {
                    this._mouseXYInGLView[0] = (this.inputsManager.mouseXY.x - this.canvasCenterX) / this.canvasCenterX;
                    this._mouseXYInGLView[1] = -(this.inputsManager.mouseXY.y - this.canvasCenterY) / this.canvasCenterY;
                    this._mouseXYInGLView[2] = 0.0;
                }
                else {
                    this._mouseXYInGLView[0] = (x - this.canvasCenterX) / this.canvasCenterX;
                    this._mouseXYInGLView[1] = -(y - this.canvasCenterY) / this.canvasCenterY;
                    this._mouseXYInGLView[2] = 0.0;
                }
                return this._mouseXYInGLView;
            };
            GameClass.prototype.gameStep = function () {
                throw new Ivane.Exceptions.NotImplemetedException();
            };
            return GameClass;
        })();
        GameClasses.GameClass = GameClass;
    })(GameClasses = Ivane.GameClasses || (Ivane.GameClasses = {}));
})(Ivane || (Ivane = {}));
/*
 * Author Ivane Gegia https://twitter.com/IvaneGegia
 */
/// <reference path="../definitions/liquidfun/liquidfun.d.ts" />
/// <reference path="Exceptions.ts" />
/// <reference path="Assertion.ts" />
var Ivane;
(function (Ivane) {
    var LiquidFunHelpers;
    (function (LiquidFunHelpers) {
        /**
         * ქმნის ფიზიკურ სამყაროს და არეგისტრირებს როგორც გლობალური ცვლადი JavaScript სივრცეში.
         * ეს ხდება მხოლოდ LiquidFun-ის JavaScript-ის ვერსიის სპეციფიკის გამო.
         *
         * @param gravity გრავიტაციის ვექტორი
         */
        function createWorldAndRegisterItAsGlobalVariable(gravity) {
            var world = new b2World(gravity);
            window["world"] = world;
            return world;
        }
        LiquidFunHelpers.createWorldAndRegisterItAsGlobalVariable = createWorldAndRegisterItAsGlobalVariable;
        function getWorld() {
            return window["world"];
        }
        LiquidFunHelpers.getWorld = getWorld;
        /**
         *
         * მსუბუქი უტილიტა ფუნქცია რომელიც ქმნის ნაწილაკების სისტემას.
         *
         * @param world_ref ფიზიკურ სამყაროზე მიმთითებელი, რომელშიც უნდა შეიქმნას ნაწილაკების სისტემა
         * @param particlesRadius ნაწილაკების რადიუსი
         * @param particleSystemDencity ნაწილაკების სისტემის სიმკვრივის კოეფიციენტი
         *
         */
        function createParticleSystem(world_ref, particlesRadius, particleSystemDencity) {
            var particleSystemDef = new b2ParticleSystemDef();
            var particleSystem = world_ref.CreateParticleSystem(particleSystemDef);
            particleSystem.SetRadius(particlesRadius);
            particleSystem.SetDensity(particleSystemDencity);
            return particleSystem;
        }
        LiquidFunHelpers.createParticleSystem = createParticleSystem;
        /**
         * წაანაცლებს მითითეუბლი ოდენობით shape-ს vertex-ებს
         *
         */
        function offsetVertexPositions(shape, offset) {
            for (var vertexIndex = 0; vertexIndex < shape.vertices.length; vertexIndex++) {
                var vertex = shape.vertices[vertexIndex];
                shape.vertices[vertexIndex].Set(vertex.x + offset[0], vertex.y + offset[1]);
            }
        }
        LiquidFunHelpers.offsetVertexPositions = offsetVertexPositions;
        /**
         *
         * ქმნის სითხის ნაწილაკების, წრის ფორმის, ჯგუფს.
         *
         * @param particleSystem_ref მიმთითებელი ნაწილაკების სისტემაზე, რომელშიც უნდა შეიქმნას ჯგუფი
         * @param groupRadius ჯგუფის ცრიული ფორმის რადიუსი
         * @param groupPosition ჯგუფის პოზიცია სამყაროში
         *
         */
        function createParticleGroupCircular(particleSystem_ref, groupRadius, groupPosition) {
            var particleGroupDef = new b2ParticleGroupDef();
            var circleShape = new b2CircleShape();
            circleShape.radius = groupRadius;
            particleGroupDef.shape = circleShape;
            particleGroupDef.angle = 0;
            particleGroupDef.position = groupPosition;
            particleGroupDef.flags = b2_waterParticle;
            var particleGroup = particleSystem_ref.CreateParticleGroup(particleGroupDef); // TODO: რეალური პარამეტრი გადავცე
            return particleGroup;
        }
        LiquidFunHelpers.createParticleGroupCircular = createParticleGroupCircular;
        /**
         * ქმნის კავშირს ორ ობიექტს შორის, სადაც A და B ობიექტებზე ფიქსირდება წერტილები რომლებიც ერთად უნდა იყონ და ამ წერტილის გარშემო
         * A და B ობიექტებს შეუძლიათ ბრუნვა.
         *
         * @param world_ref ფიზიკური სამყარო
         * @param bodyA ობიექტი A
         * @param bodyB ობიექტი B
         * @param sharedAnchorInWorldSpace წერტილი სივრცეში რომლის გარშემოც შეუძლიათ იტრიალონ A და B ობიექტებმა, ეს არის ასევე მათი საყრდენი შემაერთები წერტილი.
         *
         */
        function createRevoluteJoint(world_ref, bodyA, bodyB, sharedAnchorInWorldSpace) {
            var revoluteJointDef = new b2RevoluteJointDef();
            revoluteJointDef.InitializeAndCreate(bodyA, bodyB, sharedAnchorInWorldSpace);
            revoluteJointDef.localAnchorA = bodyA.GetLocalPoint(sharedAnchorInWorldSpace);
            revoluteJointDef.localAnchorB = bodyB.GetLocalPoint(sharedAnchorInWorldSpace);
            var revoluteJoint = world_ref.CreateJoint(revoluteJointDef);
            return revoluteJoint;
        }
        LiquidFunHelpers.createRevoluteJoint = createRevoluteJoint;
        /**
         * ქმნის მანძილის შემზღუდველს კავშირს ორ ობიექტს შორის.
         *
         * @param world_ref ფიზიკური სამყარო
         * @param bodyA პირველი ობიექტი
         * @param bodyB მეორე ობიექტი
         * @param anchorA_inLocalSpace საყრდენი წერტილი პირველი ობიექტისთვის, მისსავე ლოკალურ საკოორდინატო სივრცეში
         * @param anchorB_inLocalSpace საყრდენი წერტილი მეორე ობიექტისათვის, მისსავე ლოკალურ საკოორდინატო სივრცეში
         * @param dampingRatio სიმკვრივის კოეფიციენტი, რეკომენდებულია 1.0
         * @param frequencyHz სიმკვრივის კორექტირების სიხშირე, რეკომენდებულია 4.0
         */
        function createDistanceJoint(world_ref, bodyA, bodyB, anchorA_inLocalSpace, anchorB_inLocalSpace, dampingRatio, //1 is recomended
            frequencyHz //4 is recomended
            ) {
            var distanceJointDef = new b2DistanceJointDef();
            //Calculating b2DistanceJointDef::length
            var anchorAWorldPosition = new b2Vec2(bodyA.GetPosition().x + anchorA_inLocalSpace.x, bodyA.GetPosition().y + anchorA_inLocalSpace.y);
            var anchorBWorldPosition = new b2Vec2(bodyB.GetPosition().x + anchorB_inLocalSpace.x, bodyB.GetPosition().y + anchorB_inLocalSpace.y);
            var distanceBetweenAnchorAAndAnchorB = Math.sqrt(Math.pow(Math.abs(anchorBWorldPosition.x - anchorAWorldPosition.x), 2)
                + Math.pow(Math.abs(anchorBWorldPosition.y - anchorAWorldPosition.y), 2));
            Ivane.Assertion.DynamicAssert(distanceBetweenAnchorAAndAnchorB > .1, "value: "
                + distanceBetweenAnchorAAndAnchorB.toString() +
                " for b2DitanceJoint::length is too small");
            distanceJointDef.length = distanceBetweenAnchorAAndAnchorB;
            distanceJointDef.dampingRatio = dampingRatio;
            distanceJointDef.frequencyHz = frequencyHz;
            distanceJointDef.InitializeAndCreate(bodyA, bodyB, anchorA_inLocalSpace, anchorB_inLocalSpace);
            distanceJointDef.localAnchorA.Set(anchorA_inLocalSpace.x, anchorA_inLocalSpace.y);
            distanceJointDef.localAnchorB.Set(anchorB_inLocalSpace.x, anchorB_inLocalSpace.y);
            var distanceJoint = world_ref.CreateJoint(distanceJointDef);
            return distanceJoint;
        }
        LiquidFunHelpers.createDistanceJoint = createDistanceJoint;
        /**
         * ქმნის სხეულს რომელზეც მოქმედებს ფიზიკური ენერგიები და მათი გავლენით გადაადგილდება.
         */
        function createDynamicBody(world_ref, shapes, density, friction, position, linearDamping, angularDamping, fixedRotation, bullet, restitution, userData, filter_nullable) {
            var bodyDefinition = new b2BodyDef();
            bodyDefinition.active = true;
            bodyDefinition.position = position;
            bodyDefinition.angularDamping = angularDamping;
            bodyDefinition.linearDamping = linearDamping;
            bodyDefinition.bullet = bullet;
            bodyDefinition.type = b2_dynamicBody;
            bodyDefinition.userData = userData;
            bodyDefinition.filter = new b2Filter();
            var dynamicBody = world_ref.CreateBody(bodyDefinition);
            for (var shapeIndex = 0; shapeIndex < shapes.length; shapeIndex++) {
                var bodyFixtureDefinition = new b2FixtureDef();
                bodyFixtureDefinition.density = density;
                bodyFixtureDefinition.friction = friction;
                bodyFixtureDefinition.shape = shapes[shapeIndex];
                bodyFixtureDefinition.restitution = restitution;
                if (filter_nullable != null) {
                    bodyFixtureDefinition.filter = filter_nullable;
                }
                dynamicBody.CreateFixtureFromDef(bodyFixtureDefinition);
            }
            return dynamicBody;
        }
        LiquidFunHelpers.createDynamicBody = createDynamicBody;
        /**
         * ქმნის სხეულს რომელზეც არ მოქმედებს გრავიტაცია დას სხვა ფიზიკური ენერგიები მაგრამ დასაშვებია ჰქონდეს სიჩქარე.
         *
         */
        function createKinematicBody(world_ref, shape, friction, position, linearDamping, angularDamping, fixedRotation, bullet, restitution, userData) {
            var bodyDefinition = new b2BodyDef();
            bodyDefinition.active = true;
            bodyDefinition.position = position;
            bodyDefinition.angularDamping = angularDamping;
            bodyDefinition.linearDamping;
            bodyDefinition.bullet = bullet;
            bodyDefinition.type = b2_kinematicBody;
            bodyDefinition.userData = userData;
            var kinematicBody = world_ref.CreateBody(bodyDefinition);
            var bodyFixtureDefinition = new b2FixtureDef();
            bodyFixtureDefinition.friction = friction;
            bodyFixtureDefinition.shape = shape;
            bodyFixtureDefinition.restitution = restitution;
            kinematicBody.CreateFixtureFromDef(bodyFixtureDefinition);
            return kinematicBody;
        }
        LiquidFunHelpers.createKinematicBody = createKinematicBody;
        /**
         * ქმნის სხეულს რომელიც არ არის გათვლილი მოძრაობაზე
         */
        function createStaticBody(world_ref, shape, friction, position, restitution, userData) {
            var bodyDefinition = new b2BodyDef();
            bodyDefinition.active = true;
            bodyDefinition.position = position;
            bodyDefinition.type = b2_staticBody;
            bodyDefinition.userData = userData;
            var staticBody = world_ref.CreateBody(bodyDefinition);
            var bodyFixtureDefinition = new b2FixtureDef();
            bodyFixtureDefinition.friction = friction;
            bodyFixtureDefinition.shape = shape;
            bodyFixtureDefinition.restitution = restitution;
            staticBody.CreateFixtureFromDef(bodyFixtureDefinition);
            return staticBody;
        }
        LiquidFunHelpers.createStaticBody = createStaticBody;
        /**
         * ქმნის ფიზიკურ ობიეტს რომლის ფორმაც ისაზღვრება b2ChainShape-თი.
         *
         * @param world_ref ფიზიკის სამყაროზე მაჩვენებელი
         * @param chainPointsInLocalSpace ჯაშწვის წერტილები, გასაზღვრული ლოკალურ კოორდინატებში
         * @param bodyPosition ფიზიკური ობიექტის პოზიცია ფიზიკურ სამყაროში
         */
        function createChainShapeBody(world_ref, chainPointsInLocalSpace, bodyPosition) {
            var chainShapeBody = null;
            var chainShape = new b2ChainShape();
            chainShape.vertices = chainPointsInLocalSpace;
            chainShapeBody = Ivane.LiquidFunHelpers.createStaticBody(world_ref, chainShape, 1, new b2Vec2(0, 0), 0, null);
            if (bodyPosition != undefined) {
                chainShapeBody.SetTransform(bodyPosition, 0);
            }
            return chainShapeBody;
        }
        LiquidFunHelpers.createChainShapeBody = createChainShapeBody;
    })(LiquidFunHelpers = Ivane.LiquidFunHelpers || (Ivane.LiquidFunHelpers = {}));
})(Ivane || (Ivane = {}));
/// <reference path="../../IvaneMain.ts" />
var Ivane;
(function (Ivane) {
    var ThreeJSHelpers;
    (function (ThreeJSHelpers) {
        /**
         * წარმოადგენს THREE.Mesh -ის ტიპს რომელიც არის განკუთვნილი
         * AB, BC, CD, პრინციპით მონაკვეთების ასაღწერად რენდერისათვის.
         */
        var BufferedLinePiecesMesh = (function () {
            function BufferedLinePiecesMesh() {
            }
            BufferedLinePiecesMesh.createBufferedLines = function (vertexCount, color) {
                var bufferedLine = new BufferedLinePiecesMesh();
                var bufferGeometry = new THREE.BufferGeometry();
                var vertexArray = new Float32Array(vertexCount * 3);
                var vertexBuffer = new THREE.BufferAttribute(vertexArray, 3);
                bufferGeometry.addAttribute("position", vertexBuffer);
                var colorArray = new Float32Array(vertexCount * 4);
                var colorBuffer = new THREE.BufferAttribute(colorArray, 4);
                bufferGeometry.addAttribute("color4", colorBuffer);
                // bufferedLine.lineMesh = new THREE.Line(bufferGeometry, new THREE.LineBasicMaterial(
                //     {
                //         color: color,
                //         transparent: true
                //     }
                // ), THREE.LinePieces)
                bufferedLine.lineMesh = new THREE.Line(bufferGeometry, new THREE.ShaderMaterial({
                    fragmentShader: this.fragmentShader,
                    vertexShader: this.vertexShader,
                    // attributes: {
                    //     color4: {type: "vec4"}
                    // },
                    transparent: true
                }), THREE.LinePieces);
                // var LineStrip: LineMode;
                // var LinePieces: LineMode;
                return bufferedLine;
            };
            /**
             * აბრუნებს პოზიციების ბუფერზე მიმთითებელს.
             */
            BufferedLinePiecesMesh.prototype.getPositionBuffer = function () {
                return this.lineMesh.geometry.getAttribute("position");
            };
            BufferedLinePiecesMesh.prototype.getColor4Buffer = function () {
                return this.lineMesh.geometry.getAttribute("color4");
            };
            BufferedLinePiecesMesh.vertexShader = "\n        attribute vec4 color4;\n        varying vec4 vColor;\n        void main() {\n            vColor = color4;\n            gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 ); \n\n        }\n        ";
            BufferedLinePiecesMesh.fragmentShader = "\n        varying vec4 vColor;\n\n        void main() {\n\n            //gl_FragColor = vec4(1.0,1.0,1.0,0.5);\n            gl_FragColor = vColor;\n\n        }        \n        ";
            return BufferedLinePiecesMesh;
        })();
        ThreeJSHelpers.BufferedLinePiecesMesh = BufferedLinePiecesMesh;
    })(ThreeJSHelpers = Ivane.ThreeJSHelpers || (Ivane.ThreeJSHelpers = {}));
})(Ivane || (Ivane = {}));
/// <reference path="../../IvaneMain.ts" />
var Ivane;
(function (Ivane) {
    var ThreeJSHelpers;
    (function (ThreeJSHelpers) {
        /**
         * დამხმარე(Gizmo) გეომეტრიის გენერატორ-რენდერერი.
         */
        var GizmoRenderer = (function () {
            /**
             * @param vertexCount წვეროების ბუფერის ზომა.
             */
            function GizmoRenderer(vertexCount, color) {
                this.vertexIndex = 0;
                this.vertexCount = 0;
                this.lineColor = vec4.fromValues(1.0, 1.0, 1.0, 1.0);
                /**
                 * წრეწირის წვეროების შემნახველი სტრუქტურა, წვეწირის რენდერისათვის.
                 */
                this.circVerts = {
                    local: null,
                    world: null,
                    vec3Buf1: vec3.create(),
                    mat4Buf1: mat4.create(),
                    vertCount: 16
                };
                this.bufferedLine = ThreeJSHelpers.BufferedLinePiecesMesh.createBufferedLines(vertexCount, color ? color : 0xff0000);
                this.bufferedLine.lineMesh.frustumCulled = false;
                this.initCircleRenderingStructures();
                this.positionBuf = this.bufferedLine.getPositionBuffer();
                this.color4Buffer = this.bufferedLine.getColor4Buffer();
                this.vertexCount = this.positionBuf.length / this.positionBuf.itemSize;
            }
            GizmoRenderer.prototype.initCircleRenderingStructures = function () {
                //ბუფერების ინიციალიზაცია
                this.circVerts.local = new Array(this.circVerts.vertCount);
                this.circVerts.world = new Array(this.circVerts.vertCount);
                for (var vertIndex_1 = 0; vertIndex_1 < this.circVerts.vertCount; vertIndex_1++) {
                    this.circVerts.local[vertIndex_1] = vec3.create();
                    this.circVerts.world[vertIndex_1] = vec3.create();
                }
                vec3.set(this.circVerts.local[0], 1.0, 0.0, 0.0);
                //ეტალონური წრეწირის, ლოკალური წვეროების გამოთვლა
                var zRotMat4 = mat4.create();
                //ვატრიალებ მატრიცას z ღერძის გარშემო, კუთხით რომელიც წრეწირის წვეროებს შორისაა
                mat4.rotateZ(zRotMat4, zRotMat4, Math.PI / (this.circVerts.vertCount / 2));
                //ყოველი მომდევნო წვერო არის წინა წვერო მოტრიალებული zRotMat4-ით
                for (var vertIndex_2 = 1; vertIndex_2 < this.circVerts.vertCount; vertIndex_2++) {
                    vec3.transformMat4(this.circVerts.local[vertIndex_2], this.circVerts.local[vertIndex_2 - 1], zRotMat4);
                }
            };
            /**
             * თვლის სამყაროში განლაგებული წრეწირის წვეროებს, იყენებს this.circVerts.local წვეროებს,
             * შედეგს ინახავს this.circVerts.world-ში.
             *
             * @param radius რადიუსი
             * @param rotationZ მობრუნება z ღერძის გარშემო
             * @param posVec3World {vec3} წრეწირის ცენტრი სამყაროში
             * @return მიმთიტებელი this.circVerts.world-ზე
             */
            GizmoRenderer.prototype.computeCirleWorld = function (radius, rotationZ, posVec3World) {
                mat4.identity(this.circVerts.mat4Buf1);
                mat4.rotateZ(this.circVerts.mat4Buf1, this.circVerts.mat4Buf1, rotationZ);
                for (var vertIndex_1 = 0; vertIndex_1 < this.circVerts.vertCount; vertIndex_1++) {
                    //ვასწორებ რადიუსს 
                    vec3.scale(this.circVerts.world[vertIndex_1], this.circVerts.local[vertIndex_1], radius);
                    //ვატრიალებ წვეროებს
                    vec3.transformMat4(this.circVerts.world[vertIndex_1], this.circVerts.world[vertIndex_1], this.circVerts.mat4Buf1);
                    //ვაადგილებ წვეროებს სამყაროში.
                    vec3.add(this.circVerts.world[vertIndex_1], this.circVerts.world[vertIndex_1], posVec3World);
                }
                return this.circVerts.world;
            };
            /**
             * აბრუნებშ renderable სცენის ობიექტს
             */
            GizmoRenderer.prototype.getRenderableObject3D = function () {
                return this.bufferedLine.lineMesh;
            };
            /**
             * ამზადებს ბუფერებს და სხვა ცვლადებს გეომეტრიის გენერაციისათვის.
             */
            GizmoRenderer.prototype.begin = function () {
                this.vertexIndex = 0;
            };
            /**
             *
             * @param rotationZ კუთხე ათვლილი x ღერძიდან, საათის ისრის საწინააღმდეგოდ.
             */
            GizmoRenderer.prototype.drawCircle = function (centerX, centerY, radius, rotationZ) {
                vec3.set(this.circVerts.vec3Buf1, centerX, centerY, 0.0);
                var worldCirlceVerts = this.computeCirleWorld(radius, rotationZ, this.circVerts.vec3Buf1);
                for (var vertexIndex_2 = 0; vertexIndex_2 < this.circVerts.vertCount - 1; vertexIndex_2++) {
                    this.drawLine(this.circVerts.world[vertexIndex_2][0], this.circVerts.world[vertexIndex_2][1], this.circVerts.world[vertexIndex_2 + 1][0], this.circVerts.world[vertexIndex_2 + 1][1]);
                }
                this.drawLine(this.circVerts.world[0][0], this.circVerts.world[0][1], this.circVerts.world[this.circVerts.vertCount - 1][0], this.circVerts.world[this.circVerts.vertCount - 1][1]);
            };
            GizmoRenderer.prototype.drawLine = function (x1, y1, x2, y2) {
                this.color4Buffer.setXYZW(this.vertexIndex, this.lineColor[0], this.lineColor[1], this.lineColor[2], this.lineColor[3]);
                this.setVertexXY(x1, y1);
                this.color4Buffer.setXYZW(this.vertexIndex, this.lineColor[0], this.lineColor[1], this.lineColor[2], this.lineColor[3]);
                this.setVertexXY(x2, y2);
            };
            GizmoRenderer.prototype.setLineColor = function (r, g, b, a) {
                vec4.set(this.lineColor, r, g, b, a);
            };
            GizmoRenderer.prototype.drawLine3D = function (x1, y1, z1, x2, y2, z2) {
                this.color4Buffer.setXYZW(this.vertexIndex, this.lineColor[0], this.lineColor[1], this.lineColor[2], this.lineColor[3]);
                this.setVertexXYZ(x1, y1, z1);
                this.color4Buffer.setXYZW(this.vertexIndex, this.lineColor[0], this.lineColor[1], this.lineColor[2], this.lineColor[3]);
                this.setVertexXYZ(x2, y2, z2);
            };
            GizmoRenderer.prototype.setVertexXYZ = function (x, y, z) {
                this.positionBuf.setXYZ(this.vertexIndex, x, y, z);
                this.vertexIndex++;
            };
            /**
             * პოზიციებსი ბუფერში წერს წვეროს მონაცემებს.
             *
             * note: რეალიზაციის სიცხადის გამო არის ეს ცალკე ფუნქციად გამოყოფილი, თუმცა
             * ეს ცხადია ეფექტური არაა როცა რამდენიმე ათას წვეროს ეხება საქმე.
             *
             */
            GizmoRenderer.prototype.setVertexXY = function (x, y) {
                this.positionBuf.setXY(this.vertexIndex, x, y);
                this.vertexIndex++;
            };
            /**
             * კორექტულად ხურავს(ანულებს გამოუყენებელ წვეროებს) ბუფერებს ხატვისათვის.
             */
            GizmoRenderer.prototype.finish = function () {
                //ბუფერში დარჩენილ ჩაუწერელ წვეროებს ვანულებ, რათა არ გამოჩდნენ ეკრანზე
                for (var vertexIndex_2 = this.vertexIndex; vertexIndex_2 < this.vertexCount; vertexIndex_2++) {
                    this.positionBuf.setXYZ(vertexIndex_2, 0.0, 0.0, 0.0);
                }
                this.positionBuf.needsUpdate = true;
                this.color4Buffer.needsUpdate = true;
            };
            return GizmoRenderer;
        })();
        ThreeJSHelpers.GizmoRenderer = GizmoRenderer;
    })(ThreeJSHelpers = Ivane.ThreeJSHelpers || (Ivane.ThreeJSHelpers = {}));
})(Ivane || (Ivane = {}));
/// <reference path="../../IvaneMain.ts" />
var Ivane;
(function (Ivane) {
    var WebGL;
    (function (WebGL) {
        var Camera = (function () {
            function Camera() {
                this.viewMatrixInverted = mat4.create();
            }
            return Camera;
        })();
        WebGL.Camera = Camera;
    })(WebGL = Ivane.WebGL || (Ivane.WebGL = {}));
})(Ivane || (Ivane = {}));
/// <reference path="../../IvaneMain.ts" />
/// <reference path="Camera.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Ivane;
(function (Ivane) {
    var WebGL;
    (function (WebGL) {
        /**
         * აღწერს კამერის ობიექტს
         */
        var Camera2D = (function (_super) {
            __extends(Camera2D, _super);
            function Camera2D() {
                _super.call(this);
                this.viewRatio = 0.0;
                this.cameraMatrix = mat4.create();
                mat4.identity(this.cameraMatrix);
                this.viewScaleVector = vec3.create();
                vec3.set(this.viewScaleVector, 1.0, 1.0, 1.0);
                this.viewScale = 1.0;
                this.viewRotationVector = vec3.create();
                this.translationVector = vec3.create();
            }
            Camera2D.prototype.resetViewMatrix = function () {
                mat4.identity(this.cameraMatrix);
            };
            /**
             * ცვლის კამერის პოზიციას სამყაროში
             */
            Camera2D.prototype.setPosition = function (x, y) {
                this.resetViewMatrix();
                vec3.set(this.translationVector, -x, -y, 0.0);
                this._setScale();
                this._setPosition();
                this.computeInvertedViewMatrix();
            };
            Camera2D.prototype._setPosition = function () {
                mat4.translate(this.cameraMatrix, this.cameraMatrix, this.translationVector);
            };
            /**
             * საზღვრავს კამერის მასშტაბირებას.
             * მასშტაბირებისას გამოიყენება ხედის რეზოლუციების შეფარდება იმისთვის რომ სურათი არ დამახიჯდეს
             * არაკვარდატული ხედის შემთხვევაში.
             */
            Camera2D.prototype.setScale = function (scale) {
                this.resetViewMatrix();
                this.viewScale = scale;
                this._setScale();
                this._setPosition();
                this.computeInvertedViewMatrix();
            };
            Camera2D.prototype._setScale = function () {
                vec3.set(this.viewScaleVector, this.viewScale / this.viewRatio, this.viewScale, 1.0);
                mat4.scale(this.cameraMatrix, this.cameraMatrix, this.viewScaleVector);
            };
            Camera2D.prototype.computeInvertedViewMatrix = function () {
                mat4.invert(this.viewMatrixInverted, this.cameraMatrix);
            };
            /**
             * საზღვრავს ხედის რეზოლუციის შეფარდებას
             *
             * @param canvasWidthDividedByHeight canvas.width / canvas.height
             */
            Camera2D.prototype.setViewRatio = function (canvasWidthDividedByHeight) {
                this.viewRatio = canvasWidthDividedByHeight;
            };
            Camera2D.prototype.viewToWorld = function (out, viewPoint) {
                vec3.transformMat4(out, viewPoint, this.viewMatrixInverted);
            };
            /**
             * აბრუნებს ხედის მატრიცას რომელიც გამოიყენება gl შეიდერში
             */
            Camera2D.prototype.getCameraMatrix = function () {
                return this.cameraMatrix;
            };
            Camera2D.prototype.getCameraMatrixInverted = function () {
                return this.viewMatrixInverted;
            };
            return Camera2D;
        })(WebGL.Camera);
        WebGL.Camera2D = Camera2D;
    })(WebGL = Ivane.WebGL || (Ivane.WebGL = {}));
})(Ivane || (Ivane = {}));
/// <reference path="../../IvaneMain.ts" />
var Ivane;
(function (Ivane) {
    var WebGL;
    (function (WebGL) {
        (function (VertexAttributeElementTypes) {
            VertexAttributeElementTypes[VertexAttributeElementTypes["Float32"] = 0] = "Float32";
            VertexAttributeElementTypes[VertexAttributeElementTypes["Uint16"] = 1] = "Uint16";
        })(WebGL.VertexAttributeElementTypes || (WebGL.VertexAttributeElementTypes = {}));
        var VertexAttributeElementTypes = WebGL.VertexAttributeElementTypes;
        var VertexAttributeArray = (function () {
            function VertexAttributeArray(elementsCount, elementsPerAttribute, elementType) {
                this.elementsPerAttribute = elementsPerAttribute;
                if (elementType == VertexAttributeElementTypes.Float32) {
                    this.buffer = new Float32Array(elementsCount * this.elementsPerAttribute);
                }
                else if (elementType == VertexAttributeElementTypes.Uint16) {
                    throw new Ivane.Exceptions.NotImplemetedException();
                }
            }
            /**
             * თვლის რამდენი წვერო შეიძლება გათავსდეს ბუფერში
             */
            VertexAttributeArray.prototype.getElementsCount = function () {
                return this.buffer.length / this.elementsPerAttribute;
            };
            VertexAttributeArray.prototype.checkIfVertexNumberIsWithinBounds = function (vertexNumber) {
                var maxVertexNumber = this.getElementsCount() - 1;
                if (vertexNumber > maxVertexNumber) {
                    throw Ivane.Exceptions.VertexNumberOutOfVerexBuffer();
                }
            };
            /**
             * ბუფერში წერს შესაბამისი ნომრის მქონე წვეროს ატრიბუტის ელემენტს.
             */
            VertexAttributeArray.prototype.setElement2f = function (vertexNumber, x, y) {
                this.checkIfVertexNumberIsWithinBounds(vertexNumber);
                //წვეროს რეალური ინდექსი წვეროების ერთიან ბუფერში
                var elementIndex = vertexNumber * this.elementsPerAttribute;
                this.buffer[elementIndex] = x;
                this.buffer[elementIndex + 1] = y;
            };
            /**
             * ბუფერში წერს შესაბამისი ნომრის მქონე წვეროს ატრიბუტის ელემენტს.
             */
            VertexAttributeArray.prototype.setElement3f = function (vertexNumber, x, y, z) {
                this.checkIfVertexNumberIsWithinBounds(vertexNumber);
                //წვეროს რეალური ინდექსი წვეროების ერთიან ბუფერში
                var elementIndex = vertexNumber * this.elementsPerAttribute;
                this.buffer[elementIndex] = x;
                this.buffer[elementIndex + 1] = y;
                this.buffer[elementIndex + 2] = z;
            };
            /**
             * ბუფერში წერს შესაბამისი ნომრის მქონე წვეროს ატრიბუტის ელემენტს.
             */
            VertexAttributeArray.prototype.setElement4f = function (vertexNumber, x, y, z, a) {
                this.checkIfVertexNumberIsWithinBounds(vertexNumber);
                //წვეროს რეალური ინდექსი წვეროების ერთიან ბუფერში
                var elementIndex = vertexNumber * this.elementsPerAttribute;
                this.buffer[elementIndex] = x;
                this.buffer[elementIndex + 1] = y;
                this.buffer[elementIndex + 2] = z;
                this.buffer[elementIndex + 3] = a;
            };
            /**
             * აკოპირებს მითითებული ნომრის მქონე ელემენტის კომპონენტებს.
             */
            VertexAttributeArray.prototype.getElement2f = function (out, elementNumber) {
                //წვეროს რეალური ინდექსი წვეროების ერთიან ბუფერში
                var elementIndex = elementNumber * this.elementsPerAttribute;
                out[0] = this.buffer[elementIndex];
                out[1] = this.buffer[elementIndex + 1];
            };
            /**
             * აკოპირებს მითითებული ნომრის მქონე ელემენტის კომპონენტებს.
             */
            VertexAttributeArray.prototype.getElement4f = function (out, elementNumber) {
                //წვეროს რეალური ინდექსი წვეროების ერთიან ბუფერში
                var elementIndex = elementNumber * this.elementsPerAttribute;
                out[0] = this.buffer[elementIndex];
                out[1] = this.buffer[elementIndex + 1];
                out[2] = this.buffer[elementIndex + 2];
                out[3] = this.buffer[elementIndex + 3];
            };
            /**
             * აკოპირებს მითითებული ნომრის მქონე ელემენტის კომპონენტებს.
             */
            VertexAttributeArray.prototype.getElement3f = function (out, elementNumber) {
                //წვეროს რეალური ინდექსი წვეროების ერთიან ბუფერში
                var elementIndex = elementNumber * this.elementsPerAttribute;
                out[0] = this.buffer[elementIndex];
                out[1] = this.buffer[elementIndex + 1];
                out[2] = this.buffer[elementIndex + 2];
            };
            return VertexAttributeArray;
        })();
        WebGL.VertexAttributeArray = VertexAttributeArray;
        function createVertexUVArray2f(vertexCount) {
            return new VertexAttributeArray(vertexCount, 2, VertexAttributeElementTypes.Float32);
        }
        WebGL.createVertexUVArray2f = createVertexUVArray2f;
        function createVertexColorArray4f(vertexCount) {
            return new VertexAttributeArray(vertexCount, 4, VertexAttributeElementTypes.Float32);
        }
        WebGL.createVertexColorArray4f = createVertexColorArray4f;
        function createVertexPositionArray3f(vertexCount) {
            return new VertexAttributeArray(vertexCount, 3, VertexAttributeElementTypes.Float32);
        }
        WebGL.createVertexPositionArray3f = createVertexPositionArray3f;
    })(WebGL = Ivane.WebGL || (Ivane.WebGL = {}));
})(Ivane || (Ivane = {}));
/// <reference path="../../IvaneMain.ts" />
/// <reference path="VertexAttributeArray.ts" />
var Ivane;
(function (Ivane) {
    var WebGL;
    (function (WebGL) {
        var GeometryAttributes = (function () {
            function GeometryAttributes() {
                this.position = null;
                this.color = null;
                this.elements = null;
            }
            return GeometryAttributes;
        })();
        WebGL.GeometryAttributes = GeometryAttributes;
        /**
         * ინახავს წვეროებით აღწერის გეომეტრიას.
         * წვეროები ყოველთვის vec3 ტიპისაა ბუფერში.
         */
        var Geometry = (function () {
            /**
             * @param vertexCount წვეროების რაოდენობა
             * @param vertexIndexBufferSize წვეორების ინდექსების რაოდენობა
             */
            function Geometry(vertexCount, enableVertexColors, elementCount) {
                this.attributes = new GeometryAttributes();
                this.attributes.position = WebGL.createVertexPositionArray3f(vertexCount);
                if (enableVertexColors) {
                    this.attributes.color = WebGL.createVertexColorArray4f(vertexCount);
                }
                if (elementCount > 0) {
                    this.attributes.elements = new Uint16Array(elementCount);
                }
                this.attributes.uv1 = WebGL.createVertexUVArray2f(vertexCount);
            }
            return Geometry;
        })();
        WebGL.Geometry = Geometry;
    })(WebGL = Ivane.WebGL || (Ivane.WebGL = {}));
})(Ivane || (Ivane = {}));
/// <reference path="../../IvaneMain.ts" />
var Ivane;
(function (Ivane) {
    var WebGL;
    (function (WebGL) {
        var Transform3D = (function () {
            function Transform3D() {
                this._rotationAxis3f = vec3.create();
                this.transformMat4 = mat4.create();
                mat4.identity(this.transformMat4);
                this.translate3f = vec3.create();
                this.scale3f = vec3.create();
                vec3.set(this.scale3f, 1.0, 1.0, 1.0);
                this.rotation3f = vec3.create();
                vec3.set(this.rotation3f, 0.0, 0.0, 0.0);
                this.computeTransformMatrix();
            }
            Transform3D.prototype.getScale3f = function () {
                return this.scale3f;
            };
            Transform3D.prototype.getTranslation3f = function () {
                return this.translate3f;
            };
            Transform3D.prototype.getRotation3f = function () {
                return this.rotation3f;
            };
            Transform3D.prototype.scale = function (x, y, z) {
                vec3.set(this.scale3f, x, y, z);
                this.computeTransformMatrix();
            };
            Transform3D.prototype.translate = function (x, y, z) {
                vec3.set(this.translate3f, x, y, z);
                this.computeTransformMatrix();
            };
            Transform3D.prototype.rotate = function (x, y, z) {
                vec3.set(this.rotation3f, x, y, z);
                this.computeTransformMatrix();
            };
            Transform3D.prototype._scale = function () {
                mat4.scale(this.transformMat4, this.transformMat4, this.scale3f);
            };
            Transform3D.prototype._translate = function () {
                mat4.translate(this.transformMat4, this.transformMat4, this.translate3f);
            };
            Transform3D.prototype._rotate = function () {
                //ვატრიალებ x ღერძის გარშემო
                vec3.set(this._rotationAxis3f, 1.0, 0.0, 0.0);
                mat4.rotate(this.transformMat4, this.transformMat4, this.rotation3f[0], this._rotationAxis3f);
                //ვატრიალებ y ღერძის გარშემო
                vec3.set(this._rotationAxis3f, 0.0, 1.0, 0.0);
                mat4.rotate(this.transformMat4, this.transformMat4, this.rotation3f[1], this._rotationAxis3f);
                //ვატრიალებ z ღერძის გარშემო
                vec3.set(this._rotationAxis3f, 0.0, 0.0, 1.0);
                mat4.rotate(this.transformMat4, this.transformMat4, this.rotation3f[2], this._rotationAxis3f);
            };
            Transform3D.prototype._setTransformMatrixToIdentity = function () {
                mat4.identity(this.transformMat4);
            };
            Transform3D.prototype.computeTransformMatrix = function () {
                this._setTransformMatrixToIdentity();
                this._scale();
                this._translate();
                this._rotate();
            };
            return Transform3D;
        })();
        WebGL.Transform3D = Transform3D;
    })(WebGL = Ivane.WebGL || (Ivane.WebGL = {}));
})(Ivane || (Ivane = {}));
/// <reference path="../../IvaneMain.ts" />
/// <reference path="VertexAttributeArray.ts" />
/**
 * აღწერს WebGL-ის ბუფერს GPU-ზე
 */
var Ivane;
(function (Ivane) {
    var WebGL;
    (function (WebGL) {
        (function (GLBufferTypes) {
            GLBufferTypes[GLBufferTypes["ARRAY_BUFFER"] = 0] = "ARRAY_BUFFER";
            GLBufferTypes[GLBufferTypes["ELEMENT_ARRAY_BUFFER"] = 1] = "ELEMENT_ARRAY_BUFFER";
        })(WebGL.GLBufferTypes || (WebGL.GLBufferTypes = {}));
        var GLBufferTypes = WebGL.GLBufferTypes;
        var GLBuffer = (function () {
            /**
             * @param target gl.ARRAY_BUFFER ან gl.ELEMENT_ARRAY_BUFFER
             */
            function GLBuffer(glContext, target) {
                this.gl = glContext;
                this.glBuffer = this.gl.createBuffer();
                if (target == GLBufferTypes.ARRAY_BUFFER) {
                    this.glBufferTarget = this.gl.ARRAY_BUFFER;
                }
                if (target == GLBufferTypes.ELEMENT_ARRAY_BUFFER) {
                    this.glBufferTarget = this.gl.ELEMENT_ARRAY_BUFFER;
                }
            }
            GLBuffer.prototype.updateBufferDataForDynamicDraw = function (data) {
                this.gl.bindBuffer(this.glBufferTarget, this.glBuffer);
                this.gl.bufferData(this.glBufferTarget, data, this.gl.DYNAMIC_DRAW);
            };
            GLBuffer.prototype.updateBufferDataForStaticDraw = function (data) {
                this.gl.bindBuffer(this.glBufferTarget, this.glBuffer);
                this.gl.bufferData(this.glBufferTarget, data, this.gl.STATIC_DRAW);
            };
            GLBuffer.prototype.bindBufer = function () {
                this.gl.bindBuffer(this.glBufferTarget, this.glBuffer);
            };
            return GLBuffer;
        })();
        WebGL.GLBuffer = GLBuffer;
    })(WebGL = Ivane.WebGL || (Ivane.WebGL = {}));
})(Ivane || (Ivane = {}));
/// <reference path="../../IvaneMain.ts" />
/// <reference path="Geometry.ts" />
/// <reference path="Transform3D.ts" />
/// <reference path="GLBuffer.ts" />
var Ivane;
(function (Ivane) {
    var WebGL;
    (function (WebGL) {
        var Object3D = (function () {
            function Object3D() {
                /**
                 * მშობელი ობიექტი სცენის იერარქიაში.
                 */
                this.parent = null;
                /**
                 * ტექსტური სახელი, მაგალითად "სკამი", "ბორბალი", ...
                 */
                this.name = "";
                this.transform3d = new WebGL.Transform3D();
                this.children = new Array();
            }
            /**
             * ამატებს შვილობის ობიექტს.
             * რენდერისას, შვილობილი ობიექტის transform3d ითვლიბა მოცემული
             * ობიექტის transform3d-სთან მიმართებით. ასე შემდეგ ბოლომდე.
             */
            Object3D.prototype.addChild = function (obj) {
                this.children.push(obj);
                obj.parent = this;
            };
            /**
             * სლის შვილობილ ობიექტ obj შვლების სიიდან.
             */
            Object3D.prototype.removeChild = function (obj) {
                var childObjectIndex = -1;
                for (var counter = 0; counter < this.children.length; counter++) {
                    if (this.children[counter] == obj) {
                        childObjectIndex = counter;
                        break;
                    }
                }
                if (childObjectIndex >= 0) {
                    obj.parent = null;
                    this.removeChildAtIndex(childObjectIndex);
                }
            };
            /**
             * შვილობილი ობიექტის წაშლა ხდება index-ით.
             */
            Object3D.prototype.removeChildAtIndex = function (index) {
                this.children.splice(index, 1);
            };
            /**
             * მოძებნის შვილ ობიექტს არის თუ არა ობიექტის მითითებული სახელით და დააბრუნებს მას.
             *
             * @param name საძიებელი ობიექტის სახელი.
             */
            Object3D.prototype.getChildByName = function (name) {
                var childWithName = null;
                for (var childIndex = 0; childIndex < this.children.length; childIndex++) {
                    if (this.children[childIndex].name == name) {
                        childWithName = this.children[childIndex];
                        break;
                    }
                }
                return childWithName;
            };
            /**
             * მოძებნის შვილ ობიექტს გაართიანებს რომელთაც აქვთ მითითებული სახელი,
             * გააერთიანებს მათ მასივში და დააბრუნებს.
             *
             * @param name საძიებელი ობიექტების სახელი
             */
            Object3D.prototype.getChildrenWithName = function (name) {
                var childrenWithName = new Array();
                for (var childIndex = 0; childIndex < this.children.length; childIndex++) {
                    if (this.children[childIndex].name == name) {
                        childrenWithName.push(this.children[childIndex]);
                    }
                }
                return childrenWithName;
            };
            return Object3D;
        })();
        WebGL.Object3D = Object3D;
    })(WebGL = Ivane.WebGL || (Ivane.WebGL = {}));
})(Ivane || (Ivane = {}));
/// <reference path="../../IvaneMain.ts" />
/// <reference path="Geometry.ts" />
/// <reference path="Transform3D.ts" />
/// <reference path="GLBuffer.ts" />
/// <reference path="Object3D.ts" />
var Ivane;
(function (Ivane) {
    var WebGL;
    (function (WebGL) {
        var AttributeBuffers = (function () {
            function AttributeBuffers() {
                this.positionBuffer = null;
                this.colorBuffer = null;
                this.uv1Buffer = null;
                this.elementsBuffer = null;
            }
            return AttributeBuffers;
        })();
        WebGL.AttributeBuffers = AttributeBuffers;
        /**
         *საბოლოო ჯამში ამან უნდ შეინახოს გეომეტრია და შეიდერი რომლითაც უნდა
         * დაიხატოს გეომეტრია, ასევე ასოცირებული ტექსტურა და სხვა დეტალები
         */
        var Mesh = (function (_super) {
            __extends(Mesh, _super);
            /**
             * აინიცალიზირებს საჭირო გეომეტრიის ბუფერეს თუ წვეროების რაოდენობა მითითებულია
             *
             * @param geometry საწყისი გეომეტრია, რომლის ტრასფორმაციაც ან GPU-ზე ან CPU(საჭიროებს worldGeometry-ის)-ზე მოხდება
             * @param worldGeometry ინახავს computeWorldGeometryOnCPU მიერ geometry ტრანსფორმაციის შედეგებს
             */
            function Mesh(glContex, shader, geometry, worldGeometry) {
                _super.call(this);
                /**
                 * დროებითი ცვლადი
                 */
                this.vertex3f = vec3.create();
                /**
                 * აღნიშნავს რომ გეომეტრია დათვლილია CPU-ზე და GPU-ში შეიძლება გაიგზავნოს worldGeometry და არა geometry
                 */
                this._worldGeometryIsComputedOnCPU = false;
                this.gl = glContex;
                this.shader = shader;
                this.geometry = geometry;
                this.attributeBuffers = new AttributeBuffers();
                if (worldGeometry) {
                    this.worldGeometry = worldGeometry;
                }
            }
            Mesh.prototype.worldGeometryIsComputedOnCPU = function () {
                return this._worldGeometryIsComputedOnCPU;
            };
            /**
             * worldMatrix4-ის გამოყენებით გარდაქმნის geometry-ში არსებულ წვეროებს და ინახავს მათ worldGeometry
             */
            Mesh.prototype.transformOnCPU = function () {
                var vertexCount = this.geometry.attributes.position.getElementsCount();
                for (var vertexNumber = 0; vertexNumber < vertexCount; vertexNumber++) {
                    // მოვიპოვებ გეომეტრიის წვეროს
                    this.geometry.attributes.position.getElement3f(this.vertex3f, vertexNumber);
                    // ვახდენ წვეროს ტრანსფორმაციას
                    vec3.transformMat4(this.vertex3f, this.vertex3f, this.transform3d.transformMat4);
                    // ვინახავს გარდაქმნილ წვეროს worldGeometry-ში
                    this.worldGeometry.attributes.position.setElement3f(vertexNumber, this.vertex3f[0], this.vertex3f[1], this.vertex3f[2]);
                }
                // ვაფიქსირებ რომ worldGeometry გამოთვლილია და მზადაა გამოყენებსითვის GPU-ზე
                this._worldGeometryIsComputedOnCPU = true;
            };
            Mesh.prototype.updatePositionsOnGPU = function () {
                if (this.attributeBuffers.positionBuffer == null) {
                    this.attributeBuffers.positionBuffer = new WebGL.GLBuffer(this.gl, WebGL.GLBufferTypes.ARRAY_BUFFER);
                }
                if (this._worldGeometryIsComputedOnCPU) {
                    this.attributeBuffers.positionBuffer.updateBufferDataForDynamicDraw(this.worldGeometry.attributes.position.buffer);
                }
                else {
                    this.attributeBuffers.positionBuffer.updateBufferDataForDynamicDraw(this.geometry.attributes.position.buffer);
                }
            };
            Mesh.prototype.updateColorsGPU = function () {
                if (this.attributeBuffers.colorBuffer == null) {
                    this.attributeBuffers.colorBuffer = new WebGL.GLBuffer(this.gl, WebGL.GLBufferTypes.ARRAY_BUFFER);
                }
                this.attributeBuffers.colorBuffer.updateBufferDataForDynamicDraw(this.geometry.attributes.color.buffer);
            };
            Mesh.prototype.updateUV1OnGPU = function () {
                if (this.attributeBuffers.uv1Buffer == null) {
                    this.attributeBuffers.uv1Buffer = new WebGL.GLBuffer(this.gl, WebGL.GLBufferTypes.ARRAY_BUFFER);
                }
                this.attributeBuffers.uv1Buffer.updateBufferDataForDynamicDraw(this.geometry.attributes.uv1.buffer);
            };
            Mesh.prototype.updateElementsOnGPU = function () {
                if (this.attributeBuffers.elementsBuffer == null) {
                    this.attributeBuffers.elementsBuffer = new WebGL.GLBuffer(this.gl, WebGL.GLBufferTypes.ELEMENT_ARRAY_BUFFER);
                }
                this.attributeBuffers.elementsBuffer.updateBufferDataForDynamicDraw(this.geometry.attributes.elements);
            };
            return Mesh;
        })(WebGL.Object3D);
        WebGL.Mesh = Mesh;
    })(WebGL = Ivane.WebGL || (Ivane.WebGL = {}));
})(Ivane || (Ivane = {}));
/// <reference path="../../IvaneMain.ts" />
/// <reference path="Geometry.ts" />
/// <reference path="Transform3D.ts" />
/// <reference path="GLBuffer.ts" />
/// <reference path="Object3D.ts" />
var Ivane;
(function (Ivane) {
    var WebGL;
    (function (WebGL) {
        /**
         * კლასი რომელიც შეიცავს სტატიკურ მეთოდებს და არის
         * {Mesh} ტიპის ობიექტის შექმნის დამხმარე ფუნქციების კოლექცია.
         */
        var MeshHelpers = (function (_super) {
            __extends(MeshHelpers, _super);
            function MeshHelpers() {
                _super.apply(this, arguments);
            }
            /**
             * ქმნის 2D კვადრატს.
             */
            MeshHelpers.createQuadMesh = function (glContext, shader, width, height) {
                var mesh = new WebGL.Mesh(glContext, shader, new WebGL.Geometry(4, true, 6), new WebGL.Geometry(4, false, 0));
                var _width = 1.0 / 2.0;
                var _height = 1.0 / 2.0;
                if (width) {
                    _width = width / 2;
                }
                if (height) {
                    _height = height / 2;
                }
                mesh.geometry.attributes.position.setElement3f(0, _width, _height, 0.0);
                mesh.geometry.attributes.position.setElement3f(1, -_width, _height, 0.0);
                mesh.geometry.attributes.position.setElement3f(2, -_width, -_height, 0.0);
                mesh.geometry.attributes.position.setElement3f(3, _width, -_height, 0.0);
                mesh.geometry.attributes.color.setElement4f(0, 0.5, 0.2, 0.5, 0.5);
                mesh.geometry.attributes.color.setElement4f(1, 0.5, 0.2, 0.5, 0.5);
                mesh.geometry.attributes.color.setElement4f(2, 0.5, 0.2, 0.5, 0.5);
                mesh.geometry.attributes.color.setElement4f(3, 0.5, 0.2, 0.5, 0.5);
                mesh.geometry.attributes.elements[0] = 0;
                mesh.geometry.attributes.elements[1] = 1;
                mesh.geometry.attributes.elements[2] = 2;
                mesh.geometry.attributes.elements[3] = 0;
                mesh.geometry.attributes.elements[4] = 2;
                mesh.geometry.attributes.elements[5] = 3;
                mesh.geometry.attributes.uv1.setElement2f(0, 1.0, 1.0);
                mesh.geometry.attributes.uv1.setElement2f(1, 0.0, 1.0);
                mesh.geometry.attributes.uv1.setElement2f(2, 0.0, 0.0);
                mesh.geometry.attributes.uv1.setElement2f(3, 1.0, 0.0);
                mesh.updatePositionsOnGPU();
                mesh.updateColorsGPU();
                mesh.updateUV1OnGPU();
                mesh.updateElementsOnGPU();
                return mesh;
            };
            return MeshHelpers;
        })(WebGL.Mesh);
        WebGL.MeshHelpers = MeshHelpers;
    })(WebGL = Ivane.WebGL || (Ivane.WebGL = {}));
})(Ivane || (Ivane = {}));
/// <reference path="../../IvaneMain.ts" />
/// <reference path="Geometry.ts" />
/// <reference path="Transform3D.ts" />
/// <reference path="GLBuffer.ts" />
var Ivane;
(function (Ivane) {
    var WebGL;
    (function (WebGL) {
        var Scene = (function (_super) {
            __extends(Scene, _super);
            function Scene() {
                _super.apply(this, arguments);
            }
            return Scene;
        })(WebGL.Object3D);
        WebGL.Scene = Scene;
    })(WebGL = Ivane.WebGL || (Ivane.WebGL = {}));
})(Ivane || (Ivane = {}));
/// <reference path="../../IvaneMain.ts" />
/// <reference path="Geometry.ts" />
/// <reference path="Transform3D.ts" />
/// <reference path="Scene.ts" />
var Ivane;
(function (Ivane) {
    var WebGL;
    (function (WebGL) {
        var Renderer = (function () {
            function Renderer(glContext) {
                this.gl = glContext;
            }
            // setClearColor(colorHEX:number):void
            // {
            //     this.gl.clearColor()
            // }
            /**
             * ასუფთავებს ფერის ბუფერს
             */
            Renderer.prototype.clearColorBuffer = function () {
                this.gl.clear(this.gl.COLOR_BUFFER_BIT);
            };
            /**
             * ასუფთავებს სიღრმის ბუფერს.
             */
            Renderer.prototype.clearDepthBuffer = function () {
                this.gl.clear(this.gl.DEPTH_BUFFER_BIT);
            };
            /**
             * ასუფთავებს ფერის და სიღრვმის ბუფერს.
             */
            Renderer.prototype.clearColorAndDepthBuffers = function () {
                this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT);
            };
            /**
             * რენდერ ფუნქცია.
             */
            Renderer.prototype.render = function (renderable, camera) {
                throw new Ivane.Exceptions.NotImplemetedException();
            };
            return Renderer;
        })();
        WebGL.Renderer = Renderer;
    })(WebGL = Ivane.WebGL || (Ivane.WebGL = {}));
})(Ivane || (Ivane = {}));
/// <reference path="../../IvaneMain.ts" />
var Ivane;
(function (Ivane) {
    var WebGL;
    (function (WebGL) {
        var Texture2D = (function () {
            function Texture2D(glContext, textureImageSource) {
                this.gl = glContext;
                //ვქმნი ტექსტურის GL ობიექტს
                this.texture = this.gl.createTexture();
                this.textureImageSource = textureImageSource;
                this.loadTextureImage();
            }
            Texture2D.prototype.loadTextureImage = function () {
                var _this = this;
                this.textureImage = new Image();
                this.textureImage.onload = function () {
                    _this.handleTextureImageLoaded();
                };
                this.textureImage.src = this.textureImageSource;
            };
            Texture2D.prototype.handleTextureImageLoaded = function () {
                //ვააქტიურებს ტექსტურის ობიექტს
                this.gl.bindTexture(this.gl.TEXTURE_2D, this.texture);
                //ვთვირთავს ტექსტურის მონაცემებს, რეალურ ანუ რეალურ სურათს
                this.gl.texImage2D(this.gl.TEXTURE_2D, 0, this.gl.RGBA, this.gl.RGBA, this.gl.UNSIGNED_BYTE, this.textureImage);
                //ვსაზღვრავს გადიდების დროს გამოიყენოს წრფივი ფილტრი
                this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MAG_FILTER, this.gl.LINEAR);
                //ვსაზღვრავ რომ ტექსტურის შემცირებისას გამოიყენოს MIPMAP ტექსტურა და მასზე წრფივი ფილტრი
                this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MIN_FILTER, this.gl.LINEAR_MIPMAP_NEAREST);
                //გააქტიურებული ტექსტურის ობიექტსიათვის რომელშიც სურათი უკვე ჩატვირტულია
                //ვაგენერირებ MIPMAP ტექსტურებს
                this.gl.generateMipmap(this.gl.TEXTURE_2D);
                //მოვემულ ტექსტურის ბოეიქტს ვხსნი აქტიური ობიექტის სტატუსიდან
                //ანუ მოვრჩი ტექსტურის რედაქტირებას
                this.gl.bindTexture(this.gl.TEXTURE_2D, null);
            };
            /**
             *
             * ააქტიურებს ტექსტურას, მითითებულ GL texture unit-ში
             * gl.TEXTURE0, gl.TEXTURE1 ...
             *
             */
            Texture2D.prototype.useTexture = function (textureUnit) {
                var texUnit = this.gl.TEXTURE0;
                if (textureUnit) {
                    texUnit = textureUnit;
                }
                //ვააქტიურებს ტექსტურის სლოტს
                this.gl.activeTexture(texUnit);
                this.gl.bindTexture(this.gl.TEXTURE_2D, this.texture);
            };
            return Texture2D;
        })();
        WebGL.Texture2D = Texture2D;
    })(WebGL = Ivane.WebGL || (Ivane.WebGL = {}));
})(Ivane || (Ivane = {}));
/// <reference path="../../IvaneMain.ts" />
var Ivane;
(function (Ivane) {
    var WebGL;
    (function (WebGL) {
        var TextureBackedFramebuffer = (function () {
            function TextureBackedFramebuffer(glContext, width, height) {
                this.width = 0;
                this.height = 0;
                this.gl = glContext;
                this.width = width;
                this.height = height;
                this.frameBuffer = this.gl.createFramebuffer();
                this.bindFramebuffer();
                this.renderTexture = this.gl.createTexture();
                this.gl.bindTexture(this.gl.TEXTURE_2D, this.renderTexture);
                //ვაფიქსირებ რომ ტექსტურა y ღერძზე უნდა აყირავდეს რადგან რენდერის შედეგები ამოტრიალებულ y სივრცეში ინახება
                this.gl.pixelStorei(this.gl.UNPACK_FLIP_Y_WEBGL, true);
                this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MAG_FILTER, this.gl.LINEAR);
                this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MIN_FILTER, this.gl.LINEAR);
                //this.gl.generateMipmap(this.gl.TEXTURE_2D)
                //ვავსებ ტექსტურას "ცარიელი" მონაცემებით
                this.gl.texImage2D(this.gl.TEXTURE_2D, 0, this.gl.RGBA, width, height, 0, this.gl.RGBA, this.gl.UNSIGNED_BYTE, null);
                //კადრის ბუფერზე ვამაგრებ სიღრმის ბუფერს
                this.depthBuffer = this.gl.createRenderbuffer();
                this.gl.bindRenderbuffer(this.gl.RENDERBUFFER, this.depthBuffer);
                this.gl.renderbufferStorage(this.gl.RENDERBUFFER, this.gl.DEPTH_COMPONENT16, width, height);
                //კადრის ბუფერზე ვამაგრაებ ფერების ბუფერს
                this.gl.framebufferTexture2D(this.gl.FRAMEBUFFER, this.gl.COLOR_ATTACHMENT0, this.gl.TEXTURE_2D, this.renderTexture, 0);
                this.gl.framebufferRenderbuffer(this.gl.FRAMEBUFFER, this.gl.DEPTH_ATTACHMENT, this.gl.RENDERBUFFER, this.depthBuffer);
                //ვათავისუფლებ მიმდინარე აქტიურ ობიექტებს აქტიური მდგომარეობიდან
                this.gl.bindTexture(this.gl.TEXTURE_2D, null);
                this.gl.bindRenderbuffer(this.gl.RENDERBUFFER, null);
                this.bindMainFramebuffer();
            }
            TextureBackedFramebuffer.prototype.bindFramebuffer = function () {
                this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, this.frameBuffer);
            };
            /**
             * ძირითადი ფრეიმ ბუფერის აქტივაცია
             */
            TextureBackedFramebuffer.prototype.bindMainFramebuffer = function () {
                this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, null);
            };
            /**
             *
             * ააქტიურებს ტექსტურას, მითითებულ GL texture unit-ში
             * gl.TEXTURE0, gl.TEXTURE1 ...
             *
             */
            TextureBackedFramebuffer.prototype.useRenderTexture = function (textureUnit) {
                var texUnit = this.gl.TEXTURE0;
                if (textureUnit) {
                    texUnit = textureUnit;
                }
                //ვააქტიურებს ტექსტურის სლოტს
                this.gl.activeTexture(texUnit);
                this.gl.bindTexture(this.gl.TEXTURE_2D, this.renderTexture);
            };
            return TextureBackedFramebuffer;
        })();
        WebGL.TextureBackedFramebuffer = TextureBackedFramebuffer;
    })(WebGL = Ivane.WebGL || (Ivane.WebGL = {}));
})(Ivane || (Ivane = {}));
/// <reference path="../../IvaneMain.ts" />
/// <reference path="Texture2D.ts" />
/// <reference path="TextureBackedFramebuffer.ts" />
var Ivane;
(function (Ivane) {
    var WebGL;
    (function (WebGL) {
        var Shader = (function () {
            function Shader(glContext, vertexShaderSource, fragmentShaderSource) {
                this.uniforms = {};
                this.vertexAttributes = {};
                this.gl = glContext;
                this.vertexShaderSource = vertexShaderSource;
                this.fragmentShaderSrouce = fragmentShaderSource;
                this.compileShaders();
                this.linkShaders();
            }
            /**
             * ტვირთავს {WebGLProgram}-ს რომელიც აწერილი იყო DOM-ის ელემენტში იდენტიფიკატორით htmlElementID
             *
             *
             */
            Shader.getShaderSourceFromDOM = function (htmlElementID) {
                var shaderScript;
                var theSource;
                var currentChild;
                shaderScript = document.getElementById(htmlElementID);
                if (!shaderScript) {
                    return null;
                }
                theSource = "";
                currentChild = shaderScript.firstChild;
                //მოვიპოვებ შეიდერის სორსს
                while (currentChild) {
                    if (currentChild.nodeType == currentChild.TEXT_NODE) {
                        theSource += currentChild.textContent;
                    }
                    currentChild = currentChild.nextSibling;
                }
                return theSource;
            };
            Shader.prototype.addVertexAttribute = function (attributeName) {
                var vertexAttributeArray = this.gl.getAttribLocation(this.program, attributeName);
                this.gl.enableVertexAttribArray(vertexAttributeArray);
                this.vertexAttributes[attributeName] = vertexAttributeArray;
            };
            Shader.prototype.addUniform = function (uniformName) {
                this.uniforms[uniformName] = this.gl.getUniformLocation(this.program, uniformName);
            };
            Shader.prototype.getUniform = function (name) {
                return this.uniforms[name];
            };
            Shader.prototype.getVertexAttribute = function (name) {
                return this.vertexAttributes[name];
            };
            Shader.prototype.compileShaders = function () {
                this.fShader = this.gl.createShader(this.gl.FRAGMENT_SHADER);
                this.gl.shaderSource(this.fShader, this.fragmentShaderSrouce);
                this.gl.compileShader(this.fShader);
                this.vShader = this.gl.createShader(this.gl.VERTEX_SHADER);
                this.gl.shaderSource(this.vShader, this.vertexShaderSource);
                this.gl.compileShader(this.vShader);
                //ვამოწმებ წარმატებით დაკომპილირდა თუ არა შეიდერი
                if (!this.gl.getShaderParameter(this.vShader, this.gl.COMPILE_STATUS)) {
                    console.log("An error occurred compiling the shaders: " + this.gl.getShaderInfoLog(this.vShader));
                    return null;
                }
                //ვამოწმებ წარმატებით დაკომპილირდა თუ არა შეიდერი
                if (!this.gl.getShaderParameter(this.fShader, this.gl.COMPILE_STATUS)) {
                    console.log("An error occurred compiling the shaders: " + this.gl.getShaderInfoLog(this.fShader));
                    return null;
                }
            };
            Shader.prototype.linkShaders = function () {
                this.program = this.gl.createProgram();
                this.gl.attachShader(this.program, this.vShader);
                this.gl.attachShader(this.program, this.fShader);
                this.gl.linkProgram(this.program);
                if (!this.gl.getProgramParameter(this.program, this.gl.LINK_STATUS)) {
                    console.log("Unable to initialize the shader program.");
                }
            };
            Shader.prototype.useShaderProgram = function () {
                this.gl.useProgram(this.program);
            };
            Shader.prototype.setSampler2D = function (uniformName, texture2d) {
                if (texture2d instanceof WebGL.Texture2D) {
                    texture2d.useTexture(this.gl.TEXTURE0);
                    this.gl.uniform1i(this.uniforms[uniformName], 0);
                }
                if (texture2d instanceof WebGL.TextureBackedFramebuffer) {
                    texture2d.useRenderTexture(this.gl.TEXTURE1);
                    this.gl.uniform1i(this.uniforms[uniformName], 1);
                    console.log("render texture");
                }
            };
            Shader.attributeNames = {
                /**
                 * Vertex attributes
                 */
                vertexAttributes: {
                    position: "position",
                    color: "color",
                    uv1: "uv1"
                },
                /**
                 * Uniform attributes
                 */
                uniforms: {
                    cameraMatrix: "cameraMatrix",
                    sampler1: "sampler1"
                }
            };
            return Shader;
        })();
        WebGL.Shader = Shader;
    })(WebGL = Ivane.WebGL || (Ivane.WebGL = {}));
})(Ivane || (Ivane = {}));
/*
 * Author Ivane Gegia http://ivane.info
 */
var Ivane;
(function (Ivane) {
    var Utils;
    (function (Utils) {
        /**
         * საბასო მესიჯების ცვლის მექანიზმი.
         * გამოსადეგია მაშინ როცა ობიექტებმა ერთი მეორეს ვინაობა არ იციან მაგრამ
         * ერთი მეორეს მიერ სათქმელის მოსმენა აინტერესებთ.
         *
         * მაგალითად:
         *  საათმა შეიძლება თქვას რომ სკოლაში წასვლის დროა.
         *  ბავშვი გაიგებს ამ მესიჯს, მაგრამ შეიძლება საათის არსებობა არც იცოდეს.
         */
        var MessageDispatcher = (function () {
            function MessageDispatcher() {
                this.msgObservers = {};
            }
            MessageDispatcher.getDefault = function () {
                if (!MessageDispatcher.defaultDispatcher) {
                    MessageDispatcher.defaultDispatcher = new MessageDispatcher();
                }
                return MessageDispatcher.defaultDispatcher;
            };
            /**
             * ამატების მესიჯის მსმენელს, მსმენელთა სიაშია.
             *
             * @param messageId მესიჯის იდენტიფიკატორი
             * @param observerCallback ფუნქცია რომელიც გამოიძახება მესიჯის საპასუხოდ
             * @param sender მესიჯის გამომგზავნელი ობიექტი, თუ მაგალითად საჭიროა კონკრეტული რომელიმე ობიექტის მიერ მესიჯების მიღება
             */
            MessageDispatcher.prototype.addMessageObserver = function (messageId, observerCallback, sender) {
                if (!this.msgObservers[messageId]) {
                    this.msgObservers[messageId] = new Array();
                }
                //ვამატებ მესიჯის დამმუშავებელ ფუნქციას სიაში, ასევე ფილტრს თუ ასეთი გადმოწოდა
                this.msgObservers[messageId].push({
                    msgCallbak: observerCallback,
                    senderFilter: sender ? sender : null
                });
            };
            /**
             * მესიჯის გაგზავნა
             *
             * @param messageId მესიჯის იდენტიფიკატორი
             * @param sender მესიჯის გამომგზავნელი
             */
            MessageDispatcher.prototype.postMessage = function (messageId, sender) {
                if (this.msgObservers[messageId]) {
                    for (var observerIndex = 0; observerIndex < this.msgObservers[messageId].length; observerIndex++) {
                        var msgObserver = this.msgObservers[messageId][observerIndex];
                        if (msgObserver != null && msgObserver.senderFilter != null && msgObserver.senderFilter === sender) {
                            msgObserver.msgCallbak(messageId, sender);
                        }
                        else if (msgObserver != null && msgObserver.senderFilter == null) {
                            msgObserver.msgCallbak(messageId, sender);
                        }
                    }
                }
            };
            /**
             * წაშლის მითითებული მესიჯის კონკრეტულ მსმენელს სიიდან
             *
             * @param messageId მესიჯის იდენტიფიკატორი
             * @param observerCallback მესიჯის მსმენელი
             */
            MessageDispatcher.prototype.removeObserver = function (messageId, observerCallback) {
                var msgObservers = this.msgObservers[messageId];
                for (var observerIndex = 0; observerIndex < msgObservers.length; observerIndex++) {
                    if (msgObservers[observerIndex].msgCallbak == observerCallback) {
                        msgObservers[observerIndex] = null;
                    }
                }
            };
            MessageDispatcher.defaultDispatcher = null;
            return MessageDispatcher;
        })();
        Utils.MessageDispatcher = MessageDispatcher;
    })(Utils = Ivane.Utils || (Ivane.Utils = {}));
})(Ivane || (Ivane = {}));
var Ivane;
(function (Ivane) {
    var RPCWS;
    (function (RPCWS) {
        /**
         * კლიენტ კლასი ivanets_srv.rpcws.RPCServer-ისთვის
         */
        var RPCClient = (function () {
            function RPCClient(remoteAddr, remotePort) {
                var _this = this;
                this.rpcCallbacks = {};
                this.ws = new WebSocket("ws://" + remoteAddr + ":" + remotePort.toString());
                this.ws.onerror = function (e) {
                    console.log(e);
                };
                this.ws.onmessage = function (e) {
                    _this.webSocketMessage(_this, e.data);
                };
            }
            RPCClient.prototype.webSocketMessage = function (client, data) {
                var msg = JSON.parse(data);
                try {
                    this.rpcCallbacks[msg.func](msg.params, client);
                }
                catch (e) { }
            };
            RPCClient.prototype.callRemoteProcedure = function (func, params) {
                var msgString = JSON.stringify({
                    func: func,
                    params: params
                });
                this.ws.send(msgString);
            };
            /**
             * ამატებს კლიენტის მიერ გამოძახებად ფუნქციას, მითითებული სახელით.
             */
            RPCClient.prototype.setCallback = function (callbackName, callback) {
                this.rpcCallbacks[callbackName] = callback;
            };
            return RPCClient;
        })();
        RPCWS.RPCClient = RPCClient;
    })(RPCWS = Ivane.RPCWS || (Ivane.RPCWS = {}));
})(Ivane || (Ivane = {}));
/// <dependency path="../jslib/liquidfun/liquidfun.js" />
/// <dependency path="../jslib/threejs/{thre.js|three.min.js}" />
/// <reference path="definitions/gl-matrix/gl-matrix.d.ts" />
/// <reference path="src/Types.ts" />
/// <reference path="src/AjaxHelpers.ts" />
/// <reference path="src/AnimationsManager.ts" />
/// <reference path="src/Assertion.ts" />
/// <reference path="src/CanvasInputsManager.ts" />
/// <reference path="src/DeltaTime.ts" />
/// <reference path="src/Exceptions.ts" />
/// <reference path="src/FireOnce.ts" />
/// <reference path="src/NoSelectableHTMLElement.ts" />
/// <reference path="src/EXTHTMLElement.ts" />
/// <reference path="src/MemoryHelpers.ts" />
/// <reference path="src/Math1.ts" />
/// <reference path="src/GameClassThreeJS.ts" />
/// <reference path="src/GameClassCanvas2D.ts" />
/// <reference path="src/GameClassWebGL.ts" />
/// <reference path="src/GameClass.ts" />
/// <reference path="src/LiquidFunHelpers.ts" />
/// <reference path="src/ThreeJSHelpers/ThreeJSHelpers.ts" />
/// <reference path="src/ThreeJSHelpers/BufferedLinePiecesMesh.ts" />
/// <reference path="src/ThreeJSHelpers/GizmoRenderer.ts" />
/// <reference path="src/WebGL/Camera2D.ts" />
/// <reference path="src/WebGL/Geometry.ts" />
/// <reference path="src/WebGL/Mesh.ts" />
/// <reference path="src/WebGL/MeshHelpers.ts" />
/// <reference path="src/IGeom.ts" />
/// <reference path="src/WebGL/Renderer.ts" />
/// <reference path="src/WebGL/Shader.ts" />
/// <reference path="src/WebGL/Texture2D.ts" />
/// <reference path="src/WebGL/TextureBackedFrameBuffer.ts" />
/// <reference path="src/WebGL/Transform3D.ts" />
/// <reference path="src/WebGL/VertexAttributeArray.ts" />
/// <reference path="src/MessageDispatcher" />
/// <reference path="src/RPCWS/RPCClient" />
/// main.ts
/// <reference path="ivanets/IvaneMain.ts" />
var GClass = (function (_super) {
    __extends(GClass, _super);
    function GClass() {
        var _this = this;
        _super.call(this);
        //Adding canvas element as a child of document.body for rendering content
        this.initCanvas2D(window.innerWidth, window.innerHeight, document.getElementById("canvasContainer"));
        this.aspectRatio = this.canvasElement.width / this.canvasElement.height;
        this.scene = new THREE.Scene();
        this.camera = new THREE.PerspectiveCamera(45, this.aspectRatio, 0.1, 1000);
        this.camera.position.z = 10;
        this.camera.position.y = 10;
        this.camera.lookAt(new THREE.Vector3(0.0, 0.0, 0.0));
        this.renderer = new THREE.WebGLRenderer({
            canvas: this.canvasElement,
            antialias: true
        });
        this.renderer.setClearColor(new THREE.Color(0xFFeeee), 1.0);
        this.renderer.setSize(this.canvasElement.width, this.canvasElement.height);
        this.cube = new THREE.Mesh(new THREE.BoxGeometry(1.0, 1.0, 1.0), new THREE.MeshBasicMaterial({
            color: 0xFF6666
        }));
        this.scene.add(this.cube);
        window.onresize = function (e) {
            _this.handleOnWindowResize();
        };
    }
    GClass.prototype.gameStep = function () {
        if (this.inputsManager.keyIsDown(Ivane.Inputs.KeyCodes.right_arrow)) {
            this.cube.rotation.y += 1.0 * this.deltaTime;
        }
        if (this.inputsManager.keyIsDown(Ivane.Inputs.KeyCodes.left_arrow)) {
            this.cube.rotation.y -= 1.0 * this.deltaTime;
        }
        this.renderer.clear(true, true);
        this.renderer.render(this.scene, this.camera);
    };
    GClass.prototype.handleOnWindowResize = function () {
        this.canvasElement.width = window.innerWidth;
        this.canvasElement.height = window.innerHeight;
        this.aspectRatio = this.canvasElement.width / this.canvasElement.height;
        this.camera.fov = 45;
        this.camera.aspect = this.aspectRatio;
        this.camera.updateProjectionMatrix();
        this.renderer.setSize(this.canvasElement.width, this.canvasElement.height);
    };
    return GClass;
})(Ivane.GameClasses.GameClass);
var gClass;
window.onload = function (e) {
    gClass = new GClass();
    gClass.run();
};
