/// main.ts
/// <reference path="ivanets/IvaneMain.ts" />

class GClass extends Ivane.GameClasses.GameClass
{
    scene:THREE.Scene
    camera:THREE.PerspectiveCamera
    renderer:THREE.WebGLRenderer
    cube:THREE.Mesh

    aspectRatio:number

    constructor()
    {
        super()

        //Adding canvas element as a child of document.body for rendering content
        this.initCanvas2D(window.innerWidth, window.innerHeight, document.getElementById("canvasContainer"))

        this.aspectRatio = this.canvasElement.width / this.canvasElement.height

        this.scene = new THREE.Scene()

        this.camera = new THREE.PerspectiveCamera(45, this.aspectRatio, 0.1, 1000)
        this.camera.position.z = 10
        this.camera.position.y = 10
        this.camera.lookAt(new THREE.Vector3(0.0, 0.0, 0.0))

        this.renderer = new THREE.WebGLRenderer({
            canvas: this.canvasElement,
            antialias: true
        })

        this.renderer.setClearColor(new THREE.Color(0xFFeeee), 1.0)

        this.renderer.setSize(this.canvasElement.width, this.canvasElement.height)

        this.cube = new THREE.Mesh(new THREE.BoxGeometry(1.0, 1.0, 1.0), new THREE.MeshBasicMaterial(
            {
                color: 0xFF6666
            }
        ))                

        this.scene.add(this.cube)

        window.onresize = (e)=>{
            this.handleOnWindowResize()
        }
    }

    gameStep()
    {
        if(this.inputsManager.keyIsDown(Ivane.Inputs.KeyCodes.right_arrow))
        {
            this.cube.rotation.y += 1.0 * this.deltaTime
        }

        if(this.inputsManager.keyIsDown(Ivane.Inputs.KeyCodes.left_arrow))
        {
            this.cube.rotation.y -= 1.0 * this.deltaTime
        }        

        this.renderer.clear(true, true)
        this.renderer.render(this.scene, this.camera)
    }

    handleOnWindowResize()
    {
        this.canvasElement.width = window.innerWidth
        this.canvasElement.height = window.innerHeight

        this.aspectRatio = this.canvasElement.width / this.canvasElement.height

        this.camera.fov = 45;
        this.camera.aspect = this.aspectRatio
        this.camera.updateProjectionMatrix()

        this.renderer.setSize(this.canvasElement.width, this.canvasElement.height)
    }
}

var gClass:GClass

window.onload = (e) =>
{
    gClass = new GClass()
    gClass.run()

}